package fr.hazielkaos.revixclaim.listeners;

import fr.hazielkaos.revixclaim.Claim;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Minecart;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerArmorStandManipulateEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class BlockListener implements Listener {

    private Claim plugin;

    public BlockListener(Claim plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerInteract(PlayerInteractEvent event) {

        if (event.getItem() != null) {
            Material it = event.getItem().getType();
            ItemStack ite = event.getItem();
            Player player = event.getPlayer();


            if (event.getAction() == Action.RIGHT_CLICK_BLOCK && it == Material.WOOD_SWORD) {
                swordBlockRightClick(event);
            }
            if (it == Material.STICK) {
                if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                    Block b = event.getClickedBlock();
                    World w = player.getWorld();
                    int x = b.getX();
                    int y = b.getY();
                    int z = b.getZ();
                    if (ite.hasItemMeta()) {
                        ItemMeta meta = ite.getItemMeta();
                        if (meta.hasDisplayName() && meta.hasEnchant(Enchantment.LURE)) {
                            if (meta.getDisplayName().equalsIgnoreCase("Claim selector")) {
                                plugin.hm.addPos1(player, x, y, z, w);
                            } else {
                                handleBlockRightClick(event);
                            }
                        } else {
                            handleBlockRightClick(event);
                        }
                    } else {
                        handleBlockRightClick(event);
                    }
                }
                if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
                    Block b = event.getClickedBlock();
                    World w = player.getWorld();
                    int x = b.getX();
                    int y = b.getY();
                    int z = b.getZ();
                    if (ite.hasItemMeta()) {
                        ItemMeta meta = ite.getItemMeta();
                        if (meta.hasDisplayName() && meta.hasEnchant(Enchantment.LURE)) {
                            if (meta.getDisplayName().equalsIgnoreCase("Claim selector")) {
                                plugin.hm.addPos2(player, x, y, z, w);
                                event.setCancelled(true);
                            }
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                }
            }
        }

        if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            handleBlockRightClick(event);
        }

    }

    private void swordBlockRightClick(PlayerInteractEvent event) {

        Player player = event.getPlayer();

        if (player.isSneaking()) {
            if (plugin.um.IsRegionHere(player.getLocation())) {
                String regionName = plugin.um.getRegionNameLoc(player.getLocation());
                if (plugin.um.isOwner(player, regionName) || plugin.um.hasPlayerBypass(player)) {
                    if (plugin.rm.isConfExist(regionName)) {
                        plugin.gm.openRegionEditorInventory(player, regionName);
                        return;
                    } else {
                        plugin.rm.createConf(regionName);
                        plugin.gm.openRegionEditorInventory(player, regionName);
                        return;
                    }
                } else {
                    plugin.um.InfoRegion(player);
                    return;
                }
            } else {
                plugin.um.InfoRegion(player);
                return;
            }
        } else {
            plugin.um.InfoRegion(player);
            return;
        }

    }

    private void handleBlockRightClick(PlayerInteractEvent event) {

        Player player = event.getPlayer();
        Block block = event.getClickedBlock();
        Material bm = block.getType();
        Location bl = block.getLocation();

        if (bm == Material.WOODEN_DOOR || bm == Material.ACACIA_DOOR || bm == Material.BIRCH_DOOR
                || bm == Material.DARK_OAK_DOOR || bm == Material.JUNGLE_DOOR || bm == Material.SPRUCE_DOOR) {
            if (plugin.um.IsRegionHere(bl)) {
                String regionName = plugin.um.getRegionNameLoc(bl);
                if (plugin.rm.isDoorOpen(regionName)) {
                    return;
                } else {
                    if (plugin.um.isOwner(player, regionName) || plugin.um.isMember(player, regionName)
                            || plugin.um.hasPlayerBypass(player)) {
                        return;
                    } else {
                        event.setCancelled(true);
                        player.sendMessage("nope");
                        return;
                    }
                }

            } else {
                return;
            }
        }

        if (bm == Material.STONE_BUTTON || bm == Material.WOOD_BUTTON) {
            if (plugin.um.IsRegionHere(bl)) {
                String regionName = plugin.um.getRegionNameLoc(bl);
                if (plugin.rm.isButtonUse(regionName)) {
                    return;
                } else {
                    if (plugin.um.isOwner(player, regionName) || plugin.um.isMember(player, regionName)
                            || plugin.um.hasPlayerBypass(player)) {
                        return;
                    } else {
                        event.setCancelled(true);
                        player.sendMessage("nope");
                        return;
                    }
                }
            } else {
                return;
            }
        }

        if (bm == Material.LEVER) {
            if (plugin.um.IsRegionHere(bl)) {
                String regionName = plugin.um.getRegionNameLoc(bl);
                if (plugin.rm.isSwitchUse(regionName)) {
                    return;
                } else {
                    if (plugin.um.isOwner(player, regionName) || plugin.um.isMember(player, regionName)
                            || plugin.um.hasPlayerBypass(player)) {
                        return;
                    } else {
                        event.setCancelled(true);
                        player.sendMessage("nope");
                        return;
                    }
                }
            } else {
                return;
            }
        }

        if (bm == Material.ACACIA_FENCE_GATE || bm == Material.BIRCH_FENCE_GATE || bm == Material.DARK_OAK_FENCE_GATE
                || bm == Material.FENCE_GATE || bm == Material.JUNGLE_FENCE_GATE || bm == Material.SPRUCE_FENCE_GATE) {
            if (plugin.um.IsRegionHere(bl)) {
                String regionName = plugin.um.getRegionNameLoc(bl);
                if (plugin.rm.isFenceGateOpen(regionName)) {
                    return;
                } else {
                    if (plugin.um.isOwner(player, regionName) || plugin.um.isMember(player, regionName)
                            || plugin.um.hasPlayerBypass(player)) {
                        return;
                    } else {
                        event.setCancelled(true);
                        player.sendMessage("nope");
                        return;
                    }
                }
            } else {
                return;
            }
        }

        if (bm == Material.TRAP_DOOR) {
            if (plugin.um.IsRegionHere(bl)) {
                String regionName = plugin.um.getRegionNameLoc(bl);
                if (plugin.rm.isTrapdoorOpen(regionName)) {
                    return;
                } else {
                    if (plugin.um.isOwner(player, regionName) || plugin.um.isMember(player, regionName)
                            || plugin.um.hasPlayerBypass(player)) {
                        return;
                    } else {
                        event.setCancelled(true);
                        player.sendMessage("nope");
                        return;
                    }
                }
            } else {
                return;
            }
        }

        if (bm == Material.ANVIL) {
            if (plugin.um.IsRegionHere(bl)) {
                String regionName = plugin.um.getRegionNameLoc(bl);
                if (plugin.rm.isAnvilAccess(regionName)) {
                    return;
                } else {
                    if (plugin.um.isOwner(player, regionName) || plugin.um.isMember(player, regionName)
                            || plugin.um.hasPlayerBypass(player)) {
                        return;
                    } else {
                        event.setCancelled(true);
                        player.sendMessage("nope");
                        return;
                    }
                }
            } else {
                return;
            }

        }

        if (bm == Material.BREWING_STAND) {
            if (plugin.um.IsRegionHere(bl)) {
                String regionName = plugin.um.getRegionNameLoc(bl);
                if (plugin.rm.isAlambicAccess(regionName)) {
                    return;
                } else {
                    if (plugin.um.isOwner(player, regionName) || plugin.um.isMember(player, regionName)
                            || plugin.um.hasPlayerBypass(player)) {
                        return;
                    } else {
                        event.setCancelled(true);
                        player.sendMessage("nope");
                        return;
                    }
                }
            } else {
                return;
            }
        }

        if (bm == Material.HOPPER) {
            if (plugin.um.IsRegionHere(bl)) {
                String regionName = plugin.um.getRegionNameLoc(bl);
                if (plugin.rm.isHopperAccess(regionName)) {
                    return;
                } else {
                    if (plugin.um.isOwner(player, regionName) || plugin.um.isMember(player, regionName)
                            || plugin.um.hasPlayerBypass(player)) {
                        return;
                    } else {
                        event.setCancelled(true);
                        player.sendMessage("nope");
                        return;
                    }
                }
            } else {
                return;
            }
        }

        if (bm == Material.DROPPER || bm == Material.DISPENSER) {
            if (plugin.um.IsRegionHere(bl)) {
                String regionName = plugin.um.getRegionNameLoc(bl);
                if (plugin.rm.isDispenserAccess(regionName)) {
                    return;
                } else {
                    if (plugin.um.isOwner(player, regionName) || plugin.um.isMember(player, regionName)
                            || plugin.um.hasPlayerBypass(player)) {
                        return;
                    } else {
                        event.setCancelled(true);
                        player.sendMessage("nope");
                        return;
                    }
                }
            } else {
                return;
            }
        }

        if (bm == Material.STORAGE_MINECART) {
            if (plugin.um.IsRegionHere(bl)) {
                String regionName = plugin.um.getRegionNameLoc(bl);
                if (plugin.rm.isMinecartchestAccess(regionName)) {
                    return;
                } else {
                    if (plugin.um.isOwner(player, regionName) || plugin.um.isMember(player, regionName)
                            || plugin.um.hasPlayerBypass(player)) {
                        return;
                    } else {
                        event.setCancelled(true);
                        player.sendMessage("nope");
                        return;
                    }
                }
            } else {
                return;
            }
        }

        if (bm == Material.HOPPER_MINECART) {
            if (plugin.um.IsRegionHere(bl)) {
                String regionName = plugin.um.getRegionNameLoc(bl);
                if (plugin.rm.isMinecarthopperAccess(regionName)) {
                    return;
                } else {
                    if (plugin.um.isOwner(player, regionName) || plugin.um.isMember(player, regionName)
                            || plugin.um.hasPlayerBypass(player)) {
                        return;
                    } else {
                        event.setCancelled(true);
                        player.sendMessage("nope");
                        return;
                    }
                }
            } else {
                return;
            }
        }
        return;

    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onInventoryOpen(InventoryOpenEvent event) {
        Player player = (Player) event.getPlayer();

        // start chestAccess
        if (event.getInventory().getHolder() instanceof Chest) {
            Chest c = (Chest) event.getInventory().getHolder();
            Location chestlocation = c.getLocation();
            if (plugin.um.IsRegionHere(chestlocation)) {
                String regionName = plugin.um.getRegionNameLoc(chestlocation);
                if (plugin.rm.isChestAccess(regionName)) {
                    return;
                } else {
                    if (plugin.um.isOwner(player, regionName) || plugin.um.isMember(player, regionName)
                            || plugin.um.hasPlayerBypass(player)) {
                        return;
                    } else {
                        event.setCancelled(true);
                        player.sendMessage("nope");
                        return;
                    }
                }
            } else {
                return;
            }
        }
        if (event.getInventory().getHolder() instanceof ShulkerBox) {
            ShulkerBox c = (ShulkerBox) event.getInventory().getHolder();
            Location chestlocation = c.getLocation();
            if (plugin.um.IsRegionHere(chestlocation)) {
                String regionName = plugin.um.getRegionNameLoc(chestlocation);
                if (plugin.rm.isChestAccess(regionName)) {
                    return;
                } else {
                    if (plugin.um.isOwner(player, regionName) || plugin.um.isMember(player, regionName)
                            || plugin.um.hasPlayerBypass(player)) {
                        return;
                    } else {
                        event.setCancelled(true);
                        player.sendMessage("nope");
                        return;
                    }
                }
            } else {
                return;
            }
        }
        if (event.getInventory().getHolder() instanceof DoubleChest) {
            DoubleChest c = (DoubleChest) event.getInventory().getHolder();
            Location chestlocation = c.getLocation();
            if (plugin.um.IsRegionHere(chestlocation)) {
                String regionName = plugin.um.getRegionNameLoc(chestlocation);
                if (plugin.rm.isChestAccess(regionName)) {
                    return;
                } else {
                    if (plugin.um.isOwner(player, regionName) || plugin.um.isMember(player, regionName)
                            || plugin.um.hasPlayerBypass(player)) {
                        return;
                    } else {
                        event.setCancelled(true);
                        player.sendMessage("nope");
                        return;
                    }
                }
            } else {
                return;
            }
        }
        // end chestAccess

        // start furnaceAccess
        if (event.getInventory().getHolder() instanceof Furnace) {
            Furnace f = (Furnace) event.getInventory().getHolder();
            Location furnaceLocation = f.getLocation();
            if (plugin.um.IsRegionHere(furnaceLocation)) {
                String regionName = plugin.um.getRegionNameLoc(furnaceLocation);
                if (plugin.rm.isFurnaceAccess(regionName)) {
                    return;
                } else {
                    if (plugin.um.isOwner(player, regionName) || plugin.um.isMember(player, regionName)
                            || plugin.um.hasPlayerBypass(player)) {
                        return;
                    } else {
                        event.setCancelled(true);
                        player.sendMessage("nope");
                        return;
                    }
                }
            } else {
                return;
            }
        }
        // end furnaceAccess

        if (event.getInventory().getHolder() instanceof Minecart) {
            Minecart m = (Minecart) event.getInventory().getHolder();
            Location mLoc = m.getLocation();
            if (plugin.um.IsRegionHere(mLoc)) {
                String regionName = plugin.um.getRegionNameLoc(mLoc);
                if (plugin.rm.isMinecartchestAccess(regionName)) {
                    return;
                } else {
                    if (plugin.um.isOwner(player, regionName) || plugin.um.isMember(player, regionName)
                            || plugin.um.hasPlayerBypass(player)) {
                        return;
                    } else {
                        event.setCancelled(true);
                        player.sendMessage("nope");
                        return;
                    }
                }
            } else {
                return;
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onBlockBreak(BlockBreakEvent event) {
        Block b = event.getBlock();
        Location bl = b.getLocation();
        Player player = event.getPlayer();
        int bx = bl.getBlockX();
        int by = bl.getBlockY();
        int bz = bl.getBlockZ();
        String bw = b.getWorld().getName();
        if (b.getType().equals(Material.SEA_LANTERN)) {
            if (plugin.getHolo().contains("Holo")) {
                for (String key : plugin.getHolo().getConfigurationSection("Holo").getKeys(false)) {
                    int x1 = plugin.getHolo().getInt("Holo." + key + ".pos1.x");
                    int y1 = plugin.getHolo().getInt("Holo." + key + ".pos1.y");
                    int z1 = plugin.getHolo().getInt("Holo." + key + ".pos1.z");
                    String w1 = plugin.getHolo().getString("Holo." + key + ".pos1.w");

                    int x2 = plugin.getHolo().getInt("Holo." + key + ".pos2.x");
                    int y2 = plugin.getHolo().getInt("Holo." + key + ".pos2.y");
                    int z2 = plugin.getHolo().getInt("Holo." + key + ".pos2.z");
                    String w2 = plugin.getHolo().getString("Holo." + key + ".pos2.w");

                    if ((bx == x1 && by == y1 && bz == z1 && bw == w1) ||
                            (bx == x2 && by == y2 && bz == z2 && bw == w2)) {
                        event.setCancelled(true);
                        player.sendMessage(ChatColor.AQUA + plugin.prefix + ChatColor.GREEN + "Vous ne pouvez pas détruire le marqueur de position");
                    }
                }
            }


        } else {
            return;
        }
    }

    @EventHandler
    public void manipulate(PlayerArmorStandManipulateEvent e)
    {
        if(!e.getRightClicked().isVisible())
        {
            e.setCancelled(true);
        }
    }
}
