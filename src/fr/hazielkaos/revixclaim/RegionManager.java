package fr.hazielkaos.revixclaim;

import fr.hazielkaos.revixclaim.type.RegionObject;

public class RegionManager {
	private Claim plugin;

	public RegionManager(Claim plugin) {
		this.plugin = plugin;
	}

	// GETTER
	public RegionObject getRegion(String regionName){
		for (RegionObject region : plugin.regionList)
		{
			if (region.getRegionName().equals(regionName))
			{
				return region;
			}
		}
		return null;
	}
	public boolean isChestAccess(String regionName) {
		RegionObject region = getRegion(regionName);
		return region != null && !region.getChestAccess().equals("false");
	}

	public boolean isDoorOpen(String regionName) {
		RegionObject region = getRegion(regionName);
		return region != null && !region.getDoorOpen().equals("false");
	}

	public boolean isButtonUse(String regionName) {
		RegionObject region = getRegion(regionName);
		return  region != null && !region.getButtonUse().equals("false");
	}

	public boolean isSwitchUse(String regionName) {
		RegionObject region = getRegion(regionName);
		return region != null && !region.getSwitchUse().equals("false");
	}

	public boolean isFurnaceAccess(String regionName) {
		RegionObject region = getRegion(regionName);
		return region != null && !region.getFurnaceAccess().equals("false");
	}

	public boolean isAnvilAccess(String regionName) {
		RegionObject region = getRegion(regionName);
		return region != null && !region.getAnvilAccess().equals("false");
	}

	public boolean isFenceGateOpen(String regionName) {
		RegionObject region = getRegion(regionName);
		return region != null && !region.getFenceOpen().equals("false");
	}


	public boolean isTrapdoorOpen(String regionName) {
		RegionObject region = getRegion(regionName);
		return region != null && !region.getTrapOpen().equals("false");
	}

	public boolean isAlambicAccess(String regionName) {
		RegionObject region = getRegion(regionName);
		return region != null && !region.getAlambicAccess().equals("false");
	}

	public boolean isDispenserAccess(String regionName) {
		RegionObject region = getRegion(regionName);
		return region != null && !region.getDispenserAccess().equals("false");
	}

	public boolean isHopperAccess(String regionName) {
		RegionObject region = getRegion(regionName);
		return region != null && !region.getHopperAccess().equals("false");
	}

	public boolean isMinecartchestAccess(String regionName) {
		RegionObject region = getRegion(regionName);
		return region != null && !region.getMinecartchestAccess().equals("false");
	}

	public boolean isMinecarthopperAccess(String regionName) {
		RegionObject region = getRegion(regionName);
		return region != null && !region.getMinecarthopperAccess().equals("false");
	}

	public boolean isConfExist(String regionName) {
		RegionObject region = getRegion(regionName);
		return region != null;
	}

	// SETTER
	public void createConf(String regionName) {
		String chest = "false";
		String door = "false";
		String button = "false";
		String Switch = "false";
		String furnace = "false";
		String anvil = "false";
		String fence = "false";
		String trapdoor = "false";
		String alambic = "false";
		String dispenser = "false";
		String hopper = "false";
		String mChest = "false";
		String mHopper = "false";
		RegionObject region = new RegionObject(regionName, chest, door, button, Switch, furnace, anvil, fence, trapdoor, alambic, dispenser, hopper, mChest, mHopper, plugin);
		plugin.saveRegionsObject(region);
	}

	public void toggleChestAccess(String regionName) {
		if (isConfExist(regionName)) {
			RegionObject region = getRegion(regionName);
			if (isChestAccess(regionName)) {
				region.setChestAccess("false");
				plugin.getRegions().set("Regions." + regionName + ".ChestAccess", "false");
				plugin.saveRegions();
				return;
			} else {
				region.setChestAccess("true");
				plugin.getRegions().set("Regions." + regionName + ".ChestAccess", "true");
				plugin.saveRegions();
				return;
			}
		} else {
			createConf(regionName);
			return;
		}
	}

	public void toggleDoorOpen(String regionName) {
		if (isConfExist(regionName)) {
			RegionObject region = getRegion(regionName);
			if (isDoorOpen(regionName)) {
				region.setDoorOpen("false");
				plugin.getRegions().set("Regions." + regionName + ".DoorOpen", "false");
				plugin.saveRegions();
				return;
			} else {
				region.setDoorOpen("true");
				plugin.getRegions().set("Regions." + regionName + ".DoorOpen", "true");
				plugin.saveRegions();
				return;
			}
		} else {
			createConf(regionName);
			return;
		}
	}

	public void switchButtonUse(String regionName) {
		if (isConfExist(regionName)) {
			RegionObject region = getRegion(regionName);
			if (isButtonUse(regionName)) {
				region.setButtonUse("false");
				plugin.getRegions().set("Regions." + regionName + ".ButtonUse", "false");
				plugin.saveRegions();
				return;
			} else {
				region.setButtonUse("true");
				plugin.getRegions().set("Regions." + regionName + ".ButtonUse", "true");
				plugin.saveRegions();
				return;
			}
		} else {
			createConf(regionName);
			return;
		}
	}

	public void switchSwitchUse(String regionName) {
		if (isConfExist(regionName)) {
			RegionObject region = getRegion(regionName);
			if (isSwitchUse(regionName)) {
				region.setSwitchUse("false");
				plugin.getRegions().set("Regions." + regionName + ".SwitchUse", "false");
				plugin.saveRegions();
				return;
			} else {
				region.setButtonUse("true");
				plugin.getRegions().set("Regions." + regionName + ".SwitchUse", "true");
				plugin.saveRegions();
				return;
			}
		} else {
			createConf(regionName);
			return;
		}
	}

	public void switchFurnaceAccess(String regionName) {
		if (isConfExist(regionName)) {
			RegionObject region = getRegion(regionName);
			if (isFurnaceAccess(regionName)) {
				region.setFurnaceAccess("false");
				plugin.getRegions().set("Regions." + regionName + ".FurnaceAccess", "false");
				plugin.saveRegions();
				return;
			} else {
				region.setFurnaceAccess("true");
				plugin.getRegions().set("Regions." + regionName + ".FurnaceAccess", "true");
				plugin.saveRegions();
				return;
			}
		} else {
			createConf(regionName);
			return;
		}
	}

	public void switchAnvilAccess(String regionName) {
		if (isConfExist(regionName)) {
			RegionObject region = getRegion(regionName);
			if (isAnvilAccess(regionName)) {
				region.setAnvilAccess("false");
				plugin.getRegions().set("Regions." + regionName + ".AnvilAccess", "false");
				plugin.saveRegions();
				return;
			} else {
				region.setAnvilAccess("true");
				plugin.getRegions().set("Regions." + regionName + ".AnvilAccess", "true");
				plugin.saveRegions();
				return;
			}
		} else {
			createConf(regionName);
			return;
		}
	}

	public void switchFenceGateOpen(String regionName) {
		if (isConfExist(regionName)) {
			RegionObject region = getRegion(regionName);
			if (isFenceGateOpen(regionName)) {
				region.setFenceOpen("false");
				plugin.getRegions().set("Regions." + regionName + ".FenceOpen", "false");
				plugin.saveRegions();
				return;
			} else {
				region.setFenceOpen("true");
				plugin.getRegions().set("Regions." + regionName + ".FenceOpen", "true");
				plugin.saveRegions();
				return;
			}
		} else {
			createConf(regionName);
			return;
		}
	}


	public void toggleTrapdoorOpen(String regionName) {
		if (isConfExist(regionName)) {
			RegionObject region = getRegion(regionName);
			if (isTrapdoorOpen(regionName)) {
				region.setTrapOpen("false");
				plugin.getRegions().set("Regions." + regionName + ".TrapOpen", "false");
				plugin.saveRegions();
				return;
			} else {
				region.setTrapOpen("true");
				plugin.getRegions().set("Regions." + regionName + ".TrapOpen", "true");
				plugin.saveRegions();
				return;
			}
		} else {
			createConf(regionName);
			return;
		}
	}

	public void toggleAlambicAccess(String regionName) {
		if (isConfExist(regionName)) {
			RegionObject region = getRegion(regionName);
			if (isAlambicAccess(regionName)) {
				region.setAlambicAccess("false");
				plugin.getRegions().set("Regions." + regionName + ".AlambicAccess", "false");
				plugin.saveRegions();
				return;
			} else {
				region.setAlambicAccess("true");
				plugin.getRegions().set("Regions." + regionName + ".AlambicAccess", "true");
				plugin.saveRegions();
				return;
			}
		} else {
			createConf(regionName);
			return;
		}
	}

	public void toggleDispenserAccess(String regionName) {
		if (isConfExist(regionName)) {
			RegionObject region = getRegion(regionName);
			if (isDispenserAccess(regionName)) {
				region.setDispenserAccess("false");
				plugin.getRegions().set("Regions." + regionName + ".DispenserAccess", "false");
				plugin.saveRegions();
				return;
			} else {
				region.setDispenserAccess("true");
				plugin.getRegions().set("Regions." + regionName + ".DispenserAccess", "true");
				plugin.saveRegions();
				return;
			}
		} else {
			createConf(regionName);
			return;
		}
	}

	public void toogleHopperAccess(String regionName) {
		if(isConfExist(regionName)){
			RegionObject region = getRegion(regionName);
			if(isHopperAccess(regionName)){
				region.setHopperAccess("false");
				plugin.getRegions().set("Regions." + regionName + ".HopperAccess", "false");
				plugin.saveRegions();
				return;
			}else {
				region.setHopperAccess("true");
				plugin.getRegions().set("Regions." + regionName + ".HopperAccess", "true");
				plugin.saveRegions();
				return;
			}
		}else {
			createConf(regionName);
			return;
		}
	}
	
	public void toggleMinecartchestAccess(String regionName) {
		if(isConfExist(regionName)){
			RegionObject region = getRegion(regionName);
			if(isMinecartchestAccess(regionName)){
				region.setMinecartchestAccess("false");
				plugin.getRegions().set("Regions." + regionName + ".MinecartchestAccess", "false");
				plugin.saveRegions();
				return;
			}else{
				region.setMinecartchestAccess("true");
				plugin.getRegions().set("Regions." + regionName + ".MinecartchestAccess", "true");
				plugin.saveRegions();
				return;
			}
		}else {
			createConf(regionName);
			return;
		}
	}

	public void toggleMinecarthopperAccess(String regionName) {
		if(isConfExist(regionName)){
			RegionObject region = getRegion(regionName);
			if(isMinecarthopperAccess(regionName)){
				region.setMinecarthopperAccess("false");
				plugin.getRegions().set("Regions." + regionName + ".MinecarthopperAccess", "false");
				plugin.saveRegions();
				return;
			}else {
				region.setMinecarthopperAccess("true");
				plugin.getRegions().set("Regions." + regionName + ".MinecarthopperAccess", "true");
				plugin.saveRegions();
				return;
			}
		}else{
			createConf(regionName);
			return; 
		}
	}
}
