package fr.hazielkaos.revixclaim;

import fr.hazielkaos.revixclaim.type.TicketObject;
import net.minecraft.server.v1_12_R1.NBTTagCompound;
import net.minecraft.server.v1_12_R1.NBTTagInt;
import net.minecraft.server.v1_12_R1.NBTTagString;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.Arrays;

public class GuiManager {
    private Claim plugin;

    public GuiManager(Claim plugin) {
        this.plugin = plugin;
    }

    public void openRegionEditorInventory(Player player, String regionName) {
        Inventory re = Bukkit.createInventory(player, 18, "Edition de " + regionName);
        createDisplay(Material.NETHER_STAR, re, 0, "Aide", "Clic pour afficher l'aide", regionName, "help", true);
        if (plugin.rm.isChestAccess(regionName)) {
            createDisplay(Material.CHEST, re, 1, "Coffre actif", "Clic pour bloquer l'ouverture des coffres", regionName, "chest", true);
        } else {
            createDisplay(Material.ENDER_CHEST, re, 1, "Coffre bloqué", "Clic pour autoriser l'ouverture des coffres", regionName, "chest", true);
        }
        if (plugin.rm.isDoorOpen(regionName)) {
            createDisplay(Material.WOOD_DOOR, re, 2, "Porte active", "Clic pour bloquer l'ouverture des portes", regionName, "door", true);
        } else {
            createDisplay(Material.IRON_DOOR, re, 2, "Porte bloquée", "Clic pour autoriser l'ouverture des portes", regionName, "door", true);
        }
        if (plugin.rm.isButtonUse(regionName)) {
            createDisplay(Material.WOOD_BUTTON, re, 3, "Boutton actif", "Clic pour bloquer l'utilisation des bouttons", regionName, "button", true);
        } else {
            createDisplay(Material.STONE_BUTTON, re, 3, "Boutton bloqué", "Clic pour autoriser l'utilisation des bouttons", regionName, "button", true);
        }
        if (plugin.rm.isSwitchUse(regionName)) {
            createDisplay(Material.LEVER, re, 4, "Levier actif", "Clic pour bloquer l'utilisation des leviers", regionName, "switch", true);
        } else {
            createDisplay(Material.TRIPWIRE_HOOK, re, 4, "Levier bloqué", "Clic pour autoriser l'utilisation des leviers", regionName, "switch", true);
        }
        if (plugin.rm.isFurnaceAccess(regionName)) {
            createDisplay(Material.FURNACE, re, 5, "Four actif", "Clic pour bloquer l'utilisation des fours", regionName, "furnace", true);
        } else {
            createDisplay(Material.FURNACE, re, 5, "Four bloqué", "Clic pour autoriser l'utilisation des fours", regionName, "furnace", true);
        }
        if (plugin.rm.isAnvilAccess(regionName)) {
            createDisplay(Material.ANVIL, re, 6, "Enclume active", "Clic pour bloquer l'utilsation des enclumes", regionName, "anvil", true);
        } else {
            createDisplay(Material.ANVIL, re, 6, "Enclume bloquée", "Clic pour activer l'utilisation des enclumes", regionName, "anvil", true);
        }
        if (plugin.rm.isFenceGateOpen(regionName)) {
            createDisplay(Material.FENCE_GATE, re, 7, "Portillon actif", "Clic pour bloquer l'utilisation des portillons", regionName, "fencegate", true);
        } else {
            createDisplay(Material.DARK_OAK_FENCE_GATE, re, 7, "Portillon bloqué", "Clic pour activer l'utilisation des portillons", regionName, "fencegate", true);
        }
        createDisplay(Material.BARRIER, re, 8, "Quitter", "", regionName, "close", false);
        if (plugin.rm.isDispenserAccess(regionName)) {
            createDisplay(Material.DISPENSER, re, 11, "Dispenser/droper actif", "Clic pour bloquer l'utilisation des chaudrons", regionName, "dispenser", true);
        } else {
            createDisplay(Material.DISPENSER, re, 11, "Dispenser/droper bloqué", "Clic pour activer l'utilisation des chaudrons", regionName, "dispenser", true);
        }
        if (plugin.rm.isTrapdoorOpen(regionName)) {
            createDisplay(Material.TRAP_DOOR, re, 12, "Trappe active", "Clic pour bloquer l'utilisation des trappes", regionName, "trapdoor", true);
        } else {
            createDisplay(Material.IRON_TRAPDOOR, re, 12, "Trappe bloquée", "Clic pour autoriser l'utilisation des trappes", regionName, "trapdoor", true);
        }
        if (plugin.rm.isAlambicAccess(regionName)) {
            createDisplay(Material.BREWING_STAND_ITEM, re, 13, "Alambic actif", "Clic pour bloquer l'utilisation des alambics", regionName, "alambic", true);
        } else {
            createDisplay(Material.BREWING_STAND_ITEM, re, 13, "Alambic bloqué", "Clic pour autoriser l'utilisation des alambics", regionName, "alambic", true);
        }
        if (plugin.rm.isHopperAccess(regionName)) {
            createDisplay(Material.HOPPER, re, 14, "Hopper actif", "Clic pour bloquer l'utilisation des hoppers", regionName, "hopper", true);
        } else {
            createDisplay(Material.HOPPER, re, 14, "Hopper bloqué", "Clic pour activer l'utilisation des hoppers", regionName, "hopper", true);
        }
        if (plugin.rm.isMinecartchestAccess(regionName)) {
            createDisplay(Material.STORAGE_MINECART, re, 15, "Minecart chest actif", "Clic pour bloquer l'utilisation des minecart chest", regionName, "minecartchest", true);
        } else {
            createDisplay(Material.STORAGE_MINECART, re, 15, "Minecart chest bloqué", "Clic pour activer l'utilisation des minecart chest", regionName, "minecartchest", true);
        }

        player.openInventory(re);

    }

    public void openClaimMenu(Player player) {
        Inventory cm = Bukkit.createInventory(player, 9, "Menu de RevixClaim");
        createMenuDisplay(Material.NETHER_STAR, cm, 0, "Aide de RevixClaim", "Clic pour afficher l'aide de RevixClaim", "help", true);
        createMenuDisplay(Material.EMERALD, cm, 3, "Demande de claim", "Clic pour lancer la procédure de demande de claim", "demande", false);
        createMenuDisplay(Material.EMPTY_MAP, cm, 4, "Liste des tickets", "Clic pour ouvrir le menu des tickets", "ticket", false);
        createMenuDisplay(Material.WOOD_SWORD, cm, 5, "Gestion de la zone", "Clic pour ouvrir la gestion de la zone", "editor", false);
        createMenuDisplay(Material.BARRIER, cm, 8, "Fermer", " ", "close", false);
        player.openInventory(cm);
    }

    public void openDemandeConfMenu(Player player) {
        Inventory dm = Bukkit.createInventory(player, 9, "Confirmation de la demande");
        createDisplayColored(Material.WOOL, (byte) 5, dm, 2, "Valider", "Envoi votre demande", "validate");
        createDisplayColored(Material.WOOL, (byte) 14, dm, 6, "Annuler", "Annule voter demande", "cancel");
        player.openInventory(dm);
    }

    public void openTicketListMenu(Player player, int page) {
        int nbTicket = plugin.getTickets().getInt("NbTicket");
        Inventory t = Bukkit.createInventory(player, 54, "Liste des tickets");
        int min;
        int max;
        int i = 0;
        int Slot = 0;
        max = page * 45;
        min = max - 44;

        for (TicketObject ticket : plugin.ticketsList) {
            i++;
            if (i >= min && i <= max) {
                createTicketlistDisplay(Material.MAP, t, Slot, "Ticket " + ticket.getId(), "Auteur : " + ticket.getOwner(), "Date : " + ticket.getDate(), "open", ticket.getId());
                Slot++;
            }
        }
        if (nbTicket > max) {
            createNavTicketListDisplay(Material.STAINED_GLASS_PANE, (byte) 14, t, 53, "Page suivante", " ", "next", page);
        }
        if (page > 1) {
            createNavTicketListDisplay(Material.STAINED_GLASS_PANE, (byte) 5, t, 45, "Page précédente", " ", "previous", page);
        }
        player.openInventory(t);
    }

    public void openTicketMenu(Player player, int ticketId) {
        TicketObject ticket = plugin.tm.getTicketById(ticketId);
        Inventory t =  Bukkit.createInventory(player, 9, "Ticket n° " + ticket.getId());
        createMenuDisplay(Material.NETHER_STAR, t, 0, "Aide", "Clic pour obtenir de l'aide", "help", false);
        createTeleportDisplay(Material.COMPASS, t, 1, "Teleportation", "Clic pour être téléporté au ticket", "teleport", ticket.getX(), ticket.getY(), ticket.getZ(), ticket.getWorld());
        createMenuDisplay(Material.WATCH, t, 2, "Date de création:", ticket.getDate(), " ", false);
        createHeadDisplay(ticket.getOwner(), t, 3);
        createColored2Diaplay(Material.WOOL, (byte) 14, t, 4, "Refuser", "Clic pour refuser la demande de claim", "refuse", ticket.getId());
        createColored2Diaplay(Material.WOOL, (byte) 5, t, 5, "Création automatique", "Clic pour créer la zone de la bedrock au ciel", "make", ticket.getId());;
        createColored2Diaplay(Material.WOOL, (byte) 4, t, 6, "Création manuelle", "Clic pour faire la zone de façon manuelle (WorldEdit)", "manually", ticket.getId());
        createMenuDisplay(Material.MAP, t, 7, "Retour aux tickets", "Clic pour revenir aux tickets", "ticketList", false);
        createMenuDisplay(Material.BARRIER, t, 8, "Fermer le menu", "Clic pour revenir au jeu", "close", false);
        player.openInventory(t);
    }

    public void createDisplay(Material material, Inventory inv, int Slot, String name, String lore,
                              String regionName, String action, boolean isEnchant) {
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        ArrayList<String> Lore = new ArrayList<String>();
        Lore.add(lore);
        meta.setLore(Lore);
        if (isEnchant) {
            meta.addEnchant(Enchantment.LURE, 1, false);
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        item.setItemMeta(meta);
        net.minecraft.server.v1_12_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound compound = (nmsStack.hasTag()) ? nmsStack.getTag() : new NBTTagCompound();
        compound.set("claim", new NBTTagString("TRUE"));
        compound.set("regionName", new NBTTagString(regionName));
        compound.set("action", new NBTTagString(action));
        item = CraftItemStack.asBukkitCopy(nmsStack);
        inv.setItem(Slot, item);

    }

    public void createMenuDisplay(Material material, Inventory inv, int Slot, String name, String lore,
                                  String action, boolean isEnchant) {
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        ArrayList<String> Lore = new ArrayList<String>();
        Lore.add(lore);
        meta.setLore(Lore);
        if (isEnchant) {
            meta.addEnchant(Enchantment.LURE, 1, false);
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        item.setItemMeta(meta);
        net.minecraft.server.v1_12_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound compound = (nmsStack.hasTag()) ? nmsStack.getTag() : new NBTTagCompound();
        compound.set("claim", new NBTTagString("TRUE"));
        compound.set("action", new NBTTagString(action));
        item = CraftItemStack.asBukkitCopy(nmsStack);
        inv.setItem(Slot, item);
    }

    public void createDisplayColored(Material material, byte data, Inventory inv, int Slot, String name, String lore, String action) {
        ItemStack item = new ItemStack(material, 1, data);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        ArrayList<String> Lore = new ArrayList<String>();
        Lore.add(lore);
        meta.setLore(Lore);
        item.setItemMeta(meta);
        net.minecraft.server.v1_12_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound compound = (nmsStack.hasTag()) ? nmsStack.getTag() : new NBTTagCompound();
        compound.set("claim", new NBTTagString("TRUE"));
        compound.set("action", new NBTTagString(action));
        item = CraftItemStack.asBukkitCopy(nmsStack);
        inv.setItem(Slot, item);
    }

    public void createTicketlistDisplay(Material material, Inventory inv, int Slot, String name, String lore, String lore2, String action, int id) {
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        meta.setLore(Arrays.asList(lore,
                lore2));
        item.setItemMeta(meta);
        net.minecraft.server.v1_12_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound compound = (nmsStack.hasTag()) ? nmsStack.getTag() : new NBTTagCompound();
        compound.set("action", new NBTTagString(action));
        compound.set("id", new NBTTagInt(id));
        item = CraftItemStack.asBukkitCopy(nmsStack);
        inv.setItem(Slot, item);
    }

    public void createNavTicketListDisplay(Material material, byte data, Inventory inv, int Slot, String name, String lore, String action, int page) {
        ItemStack item = new ItemStack(material, 1, data);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        ArrayList<String> Lore = new ArrayList<String>();
        Lore.add(lore);
        meta.setLore(Lore);
        item.setItemMeta(meta);
        net.minecraft.server.v1_12_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound compound = (nmsStack.hasTag()) ? nmsStack.getTag() : new NBTTagCompound();
        compound.set("action", new NBTTagString(action));
        compound.set("page", new NBTTagInt(page));
        item = CraftItemStack.asBukkitCopy(nmsStack);
        inv.setItem(Slot, item);
    }

    @SuppressWarnings( "deprecation" )
    public void createHeadDisplay(String playerName, Inventory inv, int Slot){
        ItemStack playerHead = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        SkullMeta playerHeadMeta = (SkullMeta) playerHead.getItemMeta();
        playerHeadMeta.setOwner(playerName);
        playerHeadMeta.setDisplayName("Owner : " + playerName);
        playerHead.setItemMeta(playerHeadMeta);
        inv.setItem(Slot, playerHead);
    }

    public void createTeleportDisplay(Material material, Inventory inv, int Slot, String name, String lore, String action, int x, int y , int z, String worldName){
        ItemStack item = new ItemStack(material);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        ArrayList<String> Lore = new ArrayList<>();
        Lore.add(lore);
        meta.setLore(Lore);
        item.setItemMeta(meta);
        net.minecraft.server.v1_12_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound compound = (nmsStack.hasTag()) ? nmsStack.getTag() : new NBTTagCompound();
        compound.set("action", new NBTTagString(action));
        compound.set("x", new NBTTagInt(x));
        compound.set("y", new NBTTagInt(y));
        compound.set("z", new NBTTagInt(z));
        compound.set("w", new NBTTagString(worldName));
        item = CraftItemStack.asBukkitCopy(nmsStack);
        inv.setItem(Slot, item);
    }
    public void createColored2Diaplay(Material material, byte data, Inventory inv, int Slot, String name, String lore, String action, int id) {
        ItemStack item = new ItemStack(material, 1, data);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        ArrayList<String> Lore = new ArrayList<String>();
        Lore.add(lore);
        meta.setLore(Lore);
        item.setItemMeta(meta);
        net.minecraft.server.v1_12_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound compound = (nmsStack.hasTag()) ? nmsStack.getTag() : new NBTTagCompound();
        compound.set("action", new NBTTagString(action));
        compound.set("id", new NBTTagInt(id));
        item = CraftItemStack.asBukkitCopy(nmsStack);
        inv.setItem(Slot, item);
    }
}
