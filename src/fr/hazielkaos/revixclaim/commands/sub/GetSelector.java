package fr.hazielkaos.revixclaim.commands.sub;

import org.bukkit.entity.Player;

import fr.hazielkaos.revixclaim.Claim;
import net.md_5.bungee.api.ChatColor;

public class GetSelector extends ClaimSubCommand{
	private Claim plugin = Claim.get();
	@Override
	public boolean execute(Player player, String[] values) {
		if (player.getInventory().firstEmpty() == -1){
			player.sendMessage(ChatColor.RED + plugin.prefix + "Vous devez avoir au moins un slot de libre dans votre inventaire");
			return true;
		}else {
		plugin.um.addClaimSelector(player);
		return true;
		}
	}

}
