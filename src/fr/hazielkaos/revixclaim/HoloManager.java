package fr.hazielkaos.revixclaim;

import fr.hazielkaos.revixclaim.type.HoloObject;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;


public class HoloManager {
    private Claim plugin;

    public HoloManager(Claim plugin) {
        this.plugin = plugin;
    }

    public void addPos1(Player player, int X, int Y, int Z, World world) {
        String playerName = player.getName();
        HoloObject holo = getHolo(playerName);
        Location pos1 = new Location(world, X, Y, Z);
        if(holo != null){
            if(holo.getPos1() != null){
                player.sendMessage(ChatColor.RED + plugin.prefix + "Vous avez déja définie la position 1 pour annuler votre demande" +
                        " définisser une seconde position puis choisissez l'option annuler");
                return;
            }else {
                holo.setPos1(pos1);
            }
        }else {
            holo = new HoloObject(playerName,pos1, null, plugin);
        }
        plugin.saveHoloObject(holo);
        player.sendMessage(ChatColor.AQUA + plugin.prefix + ChatColor.GREEN + "Première position enregistrée");
        Block b = pos1.getBlock();
        b.setType(Material.SEA_LANTERN);
        ArmorStand as = (ArmorStand) pos1.getWorld().spawnEntity(pos1, EntityType.ARMOR_STAND);
        as.setGravity(false);
        as.setCanPickupItems(false);
        as.setCustomName(ChatColor.GREEN + "Position 1 " + ChatColor.BLUE + playerName);
        as.setCustomNameVisible(true);
        as.setVisible(false);
        checkPos(player);
    }

    public void addPos2(Player player, int X, int Y, int Z, World world) {
        String playerName = player.getName();
        HoloObject holo = getHolo(playerName);
        Location pos2 = new Location(world, X, Y, Z);
        if(holo!=null){
            if(holo.getPos2() != null){
                player.sendMessage(ChatColor.RED + plugin.prefix + "Vous avez déja définie la position 2 pour annuler votre demande définisser la position 1 puis choissiez l'option annuler");
                return;
            }else {
                holo.setPos2(pos2);
            }
        }else {
            holo = new HoloObject(playerName, null, pos2, plugin);
        }
        plugin.saveHoloObject(holo);
        player.sendMessage(ChatColor.AQUA + plugin.prefix + ChatColor.GREEN + "Deuxième position enregistrée");
        Block b = pos2.getBlock();
        b.setType(Material.SEA_LANTERN);
        ArmorStand as = (ArmorStand) pos2.getWorld().spawnEntity(pos2, EntityType.ARMOR_STAND); //Spawn the ArmorStand
        as.setGravity(false);
        as.setCanPickupItems(false);
        as.setCustomName(ChatColor.GREEN + "Position 2 " + ChatColor.BLUE + playerName);
        as.setCustomNameVisible(true);
        as.setVisible(false);
        checkPos(player);
    }

    private void checkPos(Player player) {
        if (hasDefinePos(player)) {
            plugin.gm.openDemandeConfMenu(player);
        }
    }

    public boolean hasDefinePos(Player player) {
        String playerName = player.getName();
        HoloObject holo = getHolo(playerName);
        return holo.getPos1() != null && holo.getPos2() != null;

    }

    public HoloObject getHolo(String owner){
        for (HoloObject holo : plugin.holoList)
        {
            if (holo.getOwner().equalsIgnoreCase(owner))
            {
                return holo;
            }
        }
        return null;
    }

    public void deleteHolo(HoloObject holo){
        Location pos1 =  holo.getPos1();
        Location pos2 = holo.getPos2();
        for (Entity e : pos1.getChunk().getEntities()) {
            EntityType et = e.getType();
            if (et == EntityType.ARMOR_STAND) {
                Location eloc = e.getLocation();
                int ex = eloc.getBlockX();
                int ey = eloc.getBlockY();
                int ez = eloc.getBlockZ();
                if (ex == pos1.getBlockX() && ey == pos1.getBlockY() && ez == pos1.getBlockZ()) {
                    e.remove();
                    Block b = pos1.getBlock();
                    b.setType(Material.GRASS);
                }
            }
        }
        for (Entity e: pos2.getChunk().getEntities()){
            EntityType et = e.getType();
            if (et == EntityType.ARMOR_STAND) {
                Location eloc = e.getLocation();
                int ex = eloc.getBlockX();
                int ey = eloc.getBlockY();
                int ez = eloc.getBlockZ();
                if (ex == pos2.getBlockX() && ey == pos2.getBlockY() && ez == pos2.getBlockZ()) {
                    e.remove();
                    Block b = pos2.getBlock();
                    b.setType(Material.GRASS);
                }
            }
        }
        plugin.removeHolo(holo);
    }

}

