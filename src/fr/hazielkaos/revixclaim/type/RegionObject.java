package fr.hazielkaos.revixclaim.type;

import fr.hazielkaos.revixclaim.Claim;

public class RegionObject {
    @SuppressWarnings("unused")
    private Claim plugin;

    public RegionObject(Claim plugin) {
        this.plugin = plugin;
    }

    //FIELDS
    protected String regionName;
    protected String ChestAccess;
    protected String DoorOpen;
    protected String ButtonUse;
    protected String SwitchUse;
    protected String FurnaceAccess;
    protected String AnvilAccess;
    protected String FenceOpen;
    protected String TrapOpen;
    protected String AlambicAccess;
    protected String DispenserAccess;
    protected String HopperAccess;
    protected String MinecartchestAccess;
    protected String MinecarthopperAccess;

    //CONSTRUCTOR
    public RegionObject(String regionName,
                        String ChestAcces,
                        String DoorOpen,
                        String ButtonUse,
                        String SwitchUse,
                        String FurnaceAccess,
                        String AnvilAccess,
                        String FenceOpen,
                        String TrapOpen,
                        String AlambicAccess,
                        String DispenseAccess,
                        String HopperAccess,
                        String MinecartchestAccess,
                        String MinecarthopperAccess,
                        Claim plugin) {
        try{
        this.regionName = regionName;
        this.ChestAccess = ChestAcces;
        this.DoorOpen = DoorOpen;
        this.ButtonUse = ButtonUse;
        this.SwitchUse = SwitchUse;
        this.FurnaceAccess = FurnaceAccess;
        this.AnvilAccess = AnvilAccess;
        this.FenceOpen = FenceOpen;
        this.TrapOpen = TrapOpen;
        this.AlambicAccess = AlambicAccess;
        this.DispenserAccess = DispenseAccess;
        this.HopperAccess = HopperAccess;
        this.MinecartchestAccess = MinecartchestAccess;
        this.MinecarthopperAccess = MinecarthopperAccess;
            plugin.regionList.add(this);

        } catch (Exception ex) {
        }

    }

    //GETTERS

    public String getAlambicAccess() {
        return AlambicAccess;
    }

    public String getAnvilAccess() {
        return AnvilAccess;
    }

    public String getButtonUse() {
        return ButtonUse;
    }

    public String getChestAccess() {
        return ChestAccess;
    }

    public String getDispenserAccess() {
        return DispenserAccess;
    }

    public String getDoorOpen() {
        return DoorOpen;
    }

    public String getFenceOpen() {
        return FenceOpen;
    }

    public String getFurnaceAccess() {
        return FurnaceAccess;
    }

    public String getHopperAccess() {
        return HopperAccess;
    }

    public String getMinecartchestAccess() {
        return MinecartchestAccess;
    }

    public String getMinecarthopperAccess() {
        return MinecarthopperAccess;
    }

    public String getRegionName() {
        return regionName;
    }

    public String getSwitchUse() {
        return SwitchUse;
    }

    public String getTrapOpen() {
        return TrapOpen;
    }

    //SETTERS
    public void setAlambicAccess(String alambicAccess) {
        AlambicAccess = alambicAccess;
    }

    public void setAnvilAccess(String anvilAccess) {
        AnvilAccess = anvilAccess;
    }

    public void setButtonUse(String buttonUse) {
        ButtonUse = buttonUse;
    }

    public void setChestAccess(String chestAccess) {
        ChestAccess = chestAccess;
    }

    public void setDispenserAccess(String dispenserAccess) {
        DispenserAccess = dispenserAccess;
    }
    public void setDoorOpen(String doorOpen) {
        DoorOpen = doorOpen;
    }

    public void setFenceOpen(String fenceOpen) {
        FenceOpen = fenceOpen;
    }

    public void setFurnaceAccess(String furnaceAccess) {
        FurnaceAccess = furnaceAccess;
    }

    public void setHopperAccess(String hopperAccess) {
        HopperAccess = hopperAccess;
    }

    public void setMinecartchestAccess(String minecartchestAccess) {
        MinecartchestAccess = minecartchestAccess;
    }

    public void setMinecarthopperAccess(String minecarthopperAccess) {
        MinecarthopperAccess = minecarthopperAccess;
    }

    public void setPlugin(Claim plugin) {
        this.plugin = plugin;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public void setSwitchUse(String switchUse) {
        SwitchUse = switchUse;
    }

    public void setTrapOpen(String trapOpen) {
        TrapOpen = trapOpen;
    }
}
