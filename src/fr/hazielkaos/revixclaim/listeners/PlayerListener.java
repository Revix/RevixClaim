package fr.hazielkaos.revixclaim.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import fr.hazielkaos.revixclaim.Claim;

public class PlayerListener implements Listener {
private Claim plugin;
	
	public PlayerListener(Claim plugin)
	{
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) { 
		Player p = e.getPlayer();
		if (plugin.permission.has(p, "claim.ticket")) {
			int NbTicket = plugin.getTickets().getInt("NbTicket");
			if ( NbTicket != 0){
				p.sendMessage(ChatColor.RED + Claim.get().prefix + ChatColor.DARK_GREEN + "il y a "+ NbTicket + " ticket(s) en attente tappez " + ChatColor.GREEN + "/claim ticket " + ChatColor.DARK_GREEN + "pour afficher la liste");
			}
		}
	}
	@EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent e) {
		Player p = e.getPlayer();
		if (plugin.permission.has(p, "claim.ticket")) {
			int NbTicket = plugin.getTickets().getInt("NbTicket");
			if ( NbTicket != 0){
				p.sendMessage(ChatColor.RED + Claim.get().prefix + ChatColor.DARK_GREEN + "il y a "+ NbTicket + " ticket(s) en attente tappez " + ChatColor.GREEN + "/claim ticket " + ChatColor.DARK_GREEN + "pour afficher la liste");
			}
		}
	}

}
