package fr.hazielkaos.revixclaim.type;

import fr.hazielkaos.revixclaim.Claim;
import org.bukkit.Location;

public class HoloObject {
    @SuppressWarnings("unused")
    private Claim plugin;

    public HoloObject(Claim plugin) {
        this.plugin = plugin;
    }

    //FIELDS
    protected String owner;
    protected Location pos1;
    protected Location pos2;

    //CONSTRUCTOR
    public HoloObject(String owner, Location pos1, Location pos2, Claim plugin){
        try {
            this.owner = owner;
            this.pos1 = pos1;
            this.pos2 = pos2;
            plugin.holoList.add(this);
        } catch (Exception ex) {
        }
    }

    //GETTER
    public String getOwner() {
        return owner;
    }

    public Location getPos1() {
        return pos1;
    }

    public Location getPos2() {
        return pos2;
    }

    //SETTERS

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setPos1(Location pos1) {
        this.pos1 = pos1;
    }

    public void setPos2(Location pos2) {
        this.pos2 = pos2;
    }
}
