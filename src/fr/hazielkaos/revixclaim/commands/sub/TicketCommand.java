package fr.hazielkaos.revixclaim.commands.sub;

import fr.hazielkaos.revixclaim.type.TicketObject;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.hazielkaos.revixclaim.Claim;

public class TicketCommand extends ClaimSubCommand{
	private Claim plugin = Claim.get();
	@Override
	public boolean execute(Player player, String[] values) {
		if (plugin.tm.isTicket()) {
			plugin.gm.openTicketListMenu(player, 1);
		} else {
			player.sendMessage(net.md_5.bungee.api.ChatColor.AQUA + plugin.prefix + net.md_5.bungee.api.ChatColor.GREEN + "Il n'y a aucun ticket en attente");
		}
		return true;
	}

}
