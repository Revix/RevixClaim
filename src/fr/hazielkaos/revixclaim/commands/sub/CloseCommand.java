package fr.hazielkaos.revixclaim.commands.sub;

import fr.hazielkaos.revixclaim.type.HoloObject;
import fr.hazielkaos.revixclaim.type.TicketObject;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.hazielkaos.revixclaim.Claim;

public class CloseCommand extends ClaimSubCommand{
	private Claim plugin = Claim.get();
	private TicketObject ticket;
	@Override
	public boolean execute(Player player, String[] values) {
		ticket = plugin.tm.getTicket(values[1]);
		if (ticket == null) {
			player.sendMessage(ChatColor.RED + "~~~~~~~~~~ " + ChatColor.AQUA + Claim.get().prefix
					+ ChatColor.RED + "~~~~~~~~~~");
			player.sendMessage(ChatColor.GREEN + "Le ticket n'existe pas");
			return true;
		}
		int anciennb;
		int newnb;
		anciennb = plugin.getTickets().getInt("NbTicket");
		newnb = anciennb - 1;
		HoloObject holo = plugin.hm.getHolo(ticket.getOwner());
		plugin.hm.deleteHolo(holo);
		plugin.removeTicket(ticket);
		player.sendMessage(ChatColor.RED + "~~~~~~~~~~ " + ChatColor.AQUA + Claim.get().prefix + ChatColor.RED
				+ "~~~~~~~~~~");
		player.sendMessage(ChatColor.GREEN + "Le ticket a bien été fermé ");
		return true;
	}

}
