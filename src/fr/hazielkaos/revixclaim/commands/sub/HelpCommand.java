package fr.hazielkaos.revixclaim.commands.sub;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.hazielkaos.revixclaim.Claim;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;

public class HelpCommand extends ClaimSubCommand {

	@Override
	public boolean execute(Player player, String[] values) {

		player.sendMessage(
				ChatColor.RED + "~~~~~~~~~~~~" + ChatColor.AQUA + "RevixClaim AIDE" + ChatColor.RED + "~~~~~~~~~~~~");
		player.sendMessage(ChatColor.GREEN + "Passez votre souris sur les commandes pour plus d'informations");

		if (Claim.get().permission.has(player, "claim.need")) {
			String overlay = "Ouvre un ticket de claim à la position ou vous êtes";
			String main = "/claim demande";
			displayHelp(player, overlay, main);
		}
		if (Claim.get().permission.has(player, "claim.info")) {
			String overlay = "Affiche les informations sur la région ou vous êtes ou la région spécifiée";
			String main = "/claim info (region en option)";
			displayHelp(player, overlay, main);
		}
		if (Claim.get().permission.has(player, "claim.info")) {
			String overlay = "Ajoute un joueur a votre claim si vous en êtes l'owner";
			String main = "/claim add <nom du claim> <joueur>";
			displayHelp(player, overlay, main);
		}
		if (Claim.get().permission.has(player, "claim.info")) {
			String overlay = "Supprime un joueur de votre claim si vous en êtes l'owner";
			String main = "/claim remove <nom du claim> <joueur>";
			displayHelp(player, overlay, main);
		}
		if (Claim.get().permission.has(player, "claim.ticket")) {
			String overlay = "Affiche tous les tickets de claim en attente";
			String main = "/claim ticket";
			displayHelp(player, overlay, main);
		}
		if (Claim.get().permission.has(player, "claim.ticket.id")) {
			String overlay = "Affiche les informations du ticket";
			String main = "/claim ticket <ID>";
			displayHelp(player, overlay, main);
		}
		if (Claim.get().permission.has(player, "claim.tp")) {
			String overlay = "Se teleporte a l'endroit ou a été fait le ticket";
			String main = "/claim tp <ID>";
			displayHelp(player, overlay, main);
		}
		if (Claim.get().permission.has(player, "claim.make")) {
			String overlay = "Ferme une demande de claim apres avoir créer la région";
			String main = "/claim make <ID>";
			displayHelp(player, overlay, main);
		}
		if (Claim.get().permission.has(player, "claim.close")) {
			String overlay = "Ferme une demande de claim (refus)";
			String main = "/claim close <ID>";
			displayHelp(player, overlay, main);
		}
		

		return true;
	}

	public void displayHelp(Player player, String overlay, String main) {
		BaseComponent[] name_hovertext = new ComponentBuilder(overlay).create();
		HoverEvent name_hoverevent = new HoverEvent(HoverEvent.Action.SHOW_TEXT, name_hovertext);
		BaseComponent[] name = new ComponentBuilder("").append(ChatColor.GREEN + main).event(name_hoverevent).create();
		player.spigot().sendMessage(name);
	}

}
