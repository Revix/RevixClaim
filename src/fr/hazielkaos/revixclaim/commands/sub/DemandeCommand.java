package fr.hazielkaos.revixclaim.commands.sub;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.hazielkaos.revixclaim.Claim;

public class DemandeCommand extends ClaimSubCommand {

	@Override
	public boolean execute(Player player, String[] values) {
		if (IsTicketRunning(player) == false) {
			int ticketNumber = Claim.get().getTickets().getInt("TicketNumber");
			ticketNumber++;
			int NbTicket = Claim.get().getTickets().getInt("NbTicket");

			Date now = new Date();
			SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

			Claim.get().getTickets().set("TicketNumber", Integer.valueOf(ticketNumber));
			Claim.get().getTickets().set("NbTicket", Integer.valueOf(NbTicket + 1));
			Claim.get().getTickets().set("Tickets." + ticketNumber + ".UUID", player.getUniqueId().toString());
			Claim.get().getTickets().set("Tickets." + ticketNumber + ".PlayerName", player.getName());
			Claim.get().getTickets().set("Tickets." + ticketNumber + ".DateCreated", format.format(now));
			Claim.get().getTickets().set("Tickets." + ticketNumber + ".x", player.getLocation().getBlockX());
			Claim.get().getTickets().set("Tickets." + ticketNumber + ".y", player.getLocation().getBlockY());
			Claim.get().getTickets().set("Tickets." + ticketNumber + ".z", player.getLocation().getBlockZ());
			Claim.get().getTickets().set("Tickets." + ticketNumber + ".world", player.getWorld().getName());
			Claim.get().saveTickets();

			player.sendMessage(ChatColor.AQUA + Claim.get().prefix + ChatColor.GREEN
					+ "Votre demande a bien été enregistrée a votre position sous le n° " + ticketNumber
					+ " elle serra traitée par un modérateur rapidement . "
					+ "Afin de rendre la création de claim plus rapide, merci de bien vouloir construire des tours délimitants votre demande"
					+ " Inutile de faire la commande plusieurs fois, tout abus serra punis");
			for (Player p : Bukkit.getServer().getOnlinePlayers()) {
				if (Claim.get().permission.has(p, "claim.ticket")) {
					p.sendTitle(ChatColor.DARK_GREEN + "Nouvelle demande de claim",
							ChatColor.GREEN + "faite /claim ticket", 10, 70, 20);
					p.sendMessage(ChatColor.RED + Claim.get().prefix + ChatColor.DARK_GREEN
							+ "Nouvelle demande de claim" + ChatColor.GREEN + " faite /claim ticket");
				}
			}
			return true;
		} else {
			player.sendMessage(ChatColor.AQUA + Claim.get().prefix + ChatColor.GREEN
					+ "Vous avez déja un ticket en attente, merci d'attendre le traitement du ticket précédent");
			return true;
		}

	}

	public boolean IsTicketRunning(Player player) {
		String p = player.getName();
		String bufferMessage = "";
		for (int i = 1; i <= Claim.get().getTickets().getInt("TicketNumber"); i++) {
			if (Claim.get().getTickets().contains("Tickets." + Integer.toString(i))) {
				if (Claim.get().getTickets().getString("Tickets." + Integer.toString(i) + ".PlayerName").equalsIgnoreCase(p)){
					bufferMessage = bufferMessage + i + ", ";
				}
			}
		}
		if (bufferMessage.length() <= 0) {
			return false;
		} else {
			return true;
		}
	}

}
