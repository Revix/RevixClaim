package fr.hazielkaos.revixclaim.commands.sub;

import org.bukkit.entity.Player;

public abstract class ClaimSubCommand {
	public abstract boolean execute(Player player, String[] values); 
}
