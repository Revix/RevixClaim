package fr.hazielkaos.revixclaim.listeners;

import fr.hazielkaos.revixclaim.Claim;
import fr.hazielkaos.revixclaim.type.HoloObject;
import fr.hazielkaos.revixclaim.type.TicketObject;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryListener implements Listener {

    private Claim plugin;

    public InventoryListener(Claim plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        ItemStack clicked = event.getCurrentItem();
        Inventory inventory = event.getInventory();
        if (inventory.getName().contains("Edition")) {
            net.minecraft.server.v1_12_R1.ItemStack nmsStack1 = CraftItemStack.asNMSCopy(clicked);
            if (nmsStack1.hasTag()) {
                net.minecraft.server.v1_12_R1.NBTTagCompound c = nmsStack1.getTag();
                if (c.hasKey("action")) {
                    String action = c.getString("action");
                    String regionName = c.getString("regionName");
                    if (action.equals("close")) {
                        event.setCancelled(true);
                        player.closeInventory();
                        return;
                    }
                    if (action.equals("chest")) {
                        event.setCancelled(true);
                        plugin.rm.toggleChestAccess(regionName);
                        if (plugin.rm.isChestAccess(regionName)) {
                            plugin.gm.createDisplay(Material.CHEST, inventory, 1, "Coffre actif",
                                    "Clic pour bloquer l'ouverture des coffres", regionName, "chest", true);
                        } else {
                            plugin.gm.createDisplay(Material.ENDER_CHEST, inventory, 1, "Coffre bloqué",
                                    "Clic pour autoriser l'ouverture des coffres", regionName, "chest", true);
                        }
                        player.updateInventory();
                        return;
                    }
                    if (action.equals("door")) {
                        event.setCancelled(true);
                        plugin.rm.toggleDoorOpen(regionName);
                        if (plugin.rm.isDoorOpen(regionName)) {
                            plugin.gm.createDisplay(Material.WOOD_DOOR, inventory, 2, "Porte active",
                                    "Clic pour bloquer l'ouverture des portes", regionName, "door", true);
                        } else {
                            plugin.gm.createDisplay(Material.IRON_DOOR, inventory, 2, "Porte bloquée",
                                    "Clic pour autoriser l'ouverture des portes", regionName, "door", true);
                        }
                        player.updateInventory();
                        return;
                    }
                    if (action.equals("button")) {
                        event.setCancelled(true);
                        plugin.rm.switchButtonUse(regionName);
                        if (plugin.rm.isButtonUse(regionName)) {
                            plugin.gm.createDisplay(Material.WOOD_BUTTON, inventory, 3, "Boutton actif",
                                    "Clic pour bloquer l'utilisation des bouttons", regionName, "button", true);
                        } else {
                            plugin.gm.createDisplay(Material.STONE_BUTTON, inventory, 3, "Boutton bloqué",
                                    "Clic pour autoriser l'utilisation des bouttons", regionName, "button", true);
                        }
                        player.updateInventory();
                        return;
                    }
                    if (action.equals("switch")) {
                        event.setCancelled(true);
                        plugin.rm.switchSwitchUse(regionName);
                        if (plugin.rm.isSwitchUse(regionName)) {
                            plugin.gm.createDisplay(Material.LEVER, inventory, 4, "Levier actif",
                                    "Clic pour bloquer l'utilisation des leviers", regionName, "switch", true);
                        } else {
                            plugin.gm.createDisplay(Material.TRIPWIRE_HOOK, inventory, 4, "Levier bloqué",
                                    "Clic pour autoriser l'utilisation des leviers", regionName, "switch", true);
                        }
                        player.updateInventory();
                        return;
                    }
                    if (action.equals("furnace")) {
                        event.setCancelled(true);
                        plugin.rm.switchFurnaceAccess(regionName);
                        if (plugin.rm.isFurnaceAccess(regionName)) {
                            plugin.gm.createDisplay(Material.FURNACE, inventory, 5, "Four actif",
                                    "Clic pour bloquer l'utilisation des fours", regionName, "furnace", true);
                        } else {
                            plugin.gm.createDisplay(Material.FURNACE, inventory, 5, "Four bloqué",
                                    "Clic pour autoriser l'utilisation des fours", regionName, "furnace", true);
                        }
                        player.updateInventory();
                        return;
                    }
                    if (action.equals("anvil")) {
                        event.setCancelled(true);
                        plugin.rm.switchAnvilAccess(regionName);
                        if (plugin.rm.isAnvilAccess(regionName)) {
                            plugin.gm.createDisplay(Material.ANVIL, inventory, 6, "Enclume active",
                                    "Clic pour bloquer l'utilsation des enclumes", regionName, "anvil", true);
                        } else {
                            plugin.gm.createDisplay(Material.ANVIL, inventory, 6, "Enclume bloquée",
                                    "Clic pour activer l'utilisation des enclumes", regionName, "anvil", true);
                        }

                        player.updateInventory();
                        return;
                    }
                    if (action.equals("fencegate")) {
                        event.setCancelled(true);
                        plugin.rm.switchFenceGateOpen(regionName);
                        if (plugin.rm.isFenceGateOpen(regionName)) {
                            plugin.gm.createDisplay(Material.FENCE_GATE, inventory, 7, "Portillon actif",
                                    "Clic pour bloquer l'utilisation des portillons", regionName, "fencegate", true);
                        } else {
                            plugin.gm.createDisplay(Material.DARK_OAK_FENCE_GATE, inventory, 7, "Portillon bloqué",
                                    "Clic pour activer l'utilisation des portillons", regionName, "fencegate", true);
                        }
                        player.updateInventory();
                        return;
                    }
                    if (action.equals("dispenser")) {
                        event.setCancelled(true);
                        plugin.rm.toggleDispenserAccess(regionName);
                        if (plugin.rm.isDispenserAccess(regionName)) {
                            plugin.gm.createDisplay(Material.DISPENSER, inventory, 11, "Dispenser/droper actif",
                                    "Clic pour bloquer l'utilisation des chaudrons", regionName, "dispenser", true);
                        } else {
                            plugin.gm.createDisplay(Material.DISPENSER, inventory, 11, "Dispenser/droper bloqué",
                                    "Clic pour activer l'utilisation des chaudrons", regionName, "dispenser", true);
                        }
                        player.updateInventory();
                        return;
                    }
                    if (action.equals("trapdoor")) {
                        event.setCancelled(true);
                        plugin.rm.toggleTrapdoorOpen(regionName);
                        if (plugin.rm.isTrapdoorOpen(regionName)) {
                            plugin.gm.createDisplay(Material.TRAP_DOOR, inventory, 12, "Trappe active",
                                    "Clic pour bloquer l'utilisation des trappes", regionName, "trapdoor", true);
                        } else {
                            plugin.gm.createDisplay(Material.IRON_TRAPDOOR, inventory, 12, "Trappe bloquée",
                                    "Clic pour autoriser l'utilisation des trappes", regionName, "trapdoor", true);
                        }
                        player.updateInventory();
                        return;
                    }
                    if (action.equals("alambic")) {
                        event.setCancelled(true);
                        plugin.rm.toggleAlambicAccess(regionName);
                        if (plugin.rm.isAlambicAccess(regionName)) {
                            plugin.gm.createDisplay(Material.BREWING_STAND_ITEM, inventory, 13, "Alambic actif",
                                    "Clic pour bloquer l'utilisation des alambics", regionName, "alambic", true);
                        } else {
                            plugin.gm.createDisplay(Material.BREWING_STAND_ITEM, inventory, 13, "Alambic bloqué",
                                    "Clic pour autoriser l'utilisation des alambics", regionName, "alambic", true);
                        }
                        player.updateInventory();
                        return;
                    }
                    if (action.equals("hopper")) {
                        event.setCancelled(true);
                        plugin.rm.toogleHopperAccess(regionName);
                        if (plugin.rm.isHopperAccess(regionName)) {
                            plugin.gm.createDisplay(Material.HOPPER, inventory, 14, "Hopper actif",
                                    "Clic pour bloquer l'utilisation des hoppers", regionName, "hopper", true);
                        } else {
                            plugin.gm.createDisplay(Material.HOPPER, inventory, 14, "Hopper bloqué",
                                    "Clic pour activer l'utilisation des hoppers", regionName, "hopper", true);
                        }
                        player.updateInventory();
                        return;
                    }
                    if (action.equals("minecartchest")) {
                        event.setCancelled(true);
                        plugin.rm.toggleMinecartchestAccess(regionName);
                        if (plugin.rm.isMinecartchestAccess(regionName)) {
                            plugin.gm.createDisplay(Material.STORAGE_MINECART, inventory, 15, "Minecart chest actif",
                                    "Clic pour bloquer l'utilisation des minecart chest", regionName, "minecartchest",
                                    true);
                        } else {
                            plugin.gm.createDisplay(Material.STORAGE_MINECART, inventory, 15, "Minecart chest bloqué",
                                    "Clic pour activer l'utilisation des minecart chest", regionName, "minecartchest",
                                    true);
                        }
                        player.updateInventory();
                        return;
                    }

                    if (action.equals("help")) {
                        event.setCancelled(true);
                        player.closeInventory();
                        player.sendMessage(ChatColor.AQUA + plugin.prefix + ChatColor.GREEN + "Aide sur les flags:");
                        player.sendMessage(ChatColor.GREEN + "Les flags sont les \"Addons\" pour une région, ils permettent de définir certaines actions");
                        player.sendMessage(ChatColor.GREEN + "Vous pouvez par exemple gràace au flag bloquer l'ouverture des portes aux joueur non membre de la région");
                        player.sendMessage(ChatColor.GREEN + "Pour connaitre la liste des flags il suffit de passer la souris sur les items dans le menu gestion de la zone");
                        return;
                    }
                } else {
                    event.setCancelled(true);
                    return;
                }
            }
            event.setCancelled(true);
            return;
        }
        if (inventory.getName().contains("Menu de RevixClaim")) {
            net.minecraft.server.v1_12_R1.ItemStack nmsStack1 = CraftItemStack.asNMSCopy(clicked);
            if (nmsStack1.hasTag()) {
                net.minecraft.server.v1_12_R1.NBTTagCompound c = nmsStack1.getTag();
                if (c.hasKey("action")) {
                    String action = c.getString("action");
                    if (action.equals("help")) {
                        event.setCancelled(true);
                        player.closeInventory();
                        player.sendMessage(ChatColor.RED + "~~~~~~~~~~~~" + ChatColor.AQUA + "RevixClaim AIDE"
                                + ChatColor.RED + "~~~~~~~~~~~~");
                        player.sendMessage(ChatColor.GREEN + "commande principale : /claim");
                        player.sendMessage(
                                ChatColor.GREEN + "La plupart des actions s'effectue grace a une épée en bois");
                        player.sendMessage(ChatColor.GREEN
                                + "Pour voir les informations sur une region faite un clic droit sur un bloc dans la région");
                        player.sendMessage(ChatColor.GREEN
                                + "Pour gérer une region faite sneak + clic droit avec une épée en bois dans la region");
                        return;
                    }
                    if (action.equals("demande")) {
                        player.closeInventory();
                        if(plugin.tm.IsTicketRunning(player)){
                            player.sendMessage(ChatColor.AQUA + plugin.prefix + ChatColor.GREEN
                                    + "Vous avez déja un ticket en attente, merci d'attendre le traitement du ticket précédent");
                        }else {
                            if (player.getInventory().firstEmpty() == -1) {
                                player.sendMessage(ChatColor.RED + plugin.prefix + "Vous devez avoir au moins un slot de libre dans votre inventaire");
                            } else {
                                plugin.um.addClaimSelector(player);
                                player.sendMessage(ChatColor.AQUA + plugin.prefix + ChatColor.GREEN + "Afin de créer votre demande de claim merci de sélectionner la zone a l'aide du selecteur");
                            }
                        }
                        event.setCancelled(true);
                        return;
                    }
                    if (action.equals("ticket")) {
                        player.closeInventory();
                        if(plugin.permission.has(player, "claim.ticket")) {
                            if (plugin.tm.isTicket()) {
                                plugin.gm.openTicketListMenu(player, 1);
                            } else {
                                player.sendMessage(ChatColor.AQUA + plugin.prefix + ChatColor.GREEN + "Il n'y a aucun ticket en attente");
                            }
                        }else {
                            plugin.noperms(player);
                        }
                        event.setCancelled(true);
                        return;
                    }
                    if (action.equals("editor")) {
                        event.setCancelled(true);
                        if (plugin.um.IsRegionHere(player.getLocation())) {
                            String regionName = plugin.um.getRegionNameLoc(player.getLocation());
                            if (plugin.um.isOwner(player, regionName)) {
                                plugin.gm.openRegionEditorInventory(player, regionName);
                                return;
                            } else {
                                player.sendMessage(ChatColor.RED + plugin.prefix
                                        + "Vous devez être owner du claim pour le modifier");
                                return;
                            }
                        } else {
                            player.sendMessage(
                                    ChatColor.RED + plugin.prefix + "Il n'y a pas de region a votre emplacement");
                            return;
                        }
                    }
                    if (action.equals("close")) {
                        event.setCancelled(true);
                        player.closeInventory();
                        return;
                    }
                } else {
                    event.setCancelled(true);
                    return;
                }
            }
            event.setCancelled(true);
            return;
        }
        if (inventory.getName().contains("Confirmation de la demande")) {
            net.minecraft.server.v1_12_R1.ItemStack nmsStack1 = CraftItemStack.asNMSCopy(clicked);
            if (nmsStack1.hasTag()) {
                net.minecraft.server.v1_12_R1.NBTTagCompound c = nmsStack1.getTag();
                if (c.hasKey("action")) {
                    String action = c.getString("action");
                    if (action.equalsIgnoreCase("validate")) {
                        plugin.tm.createTicket(player);
                        plugin.um.removeClaimSelector(player);
                        player.closeInventory();
                        event.setCancelled(true);
                        return;
                    }
                    if (action.equalsIgnoreCase("cancel")) {
                        HoloObject holo = plugin.hm.getHolo(player.getName());
                        plugin.hm.deleteHolo(holo);
                        plugin.um.removeClaimSelector(player);
                        event.setCancelled(true);
                        player.closeInventory();
                        return;
                    }

                } else {
                    event.setCancelled(true);
                }
            }
            event.setCancelled(true);
            return;
        }
        if (inventory.getName().contains("Liste des tickets")) {
            net.minecraft.server.v1_12_R1.ItemStack nmsStack1 = CraftItemStack.asNMSCopy(clicked);
            if (nmsStack1.hasTag()) {
                net.minecraft.server.v1_12_R1.NBTTagCompound c = nmsStack1.getTag();
                if (c.hasKey("action")) {
                    String action = c.getString("action");
                    if(action.equalsIgnoreCase("open")){
                        event.setCancelled(true);
                        int ticket = c.getInt("id");
                        player.closeInventory();
                        plugin.gm.openTicketMenu(player, ticket);
                        return;
                    }
                    if (action.equalsIgnoreCase("next")) {
                        event.setCancelled(true);
                        player.closeInventory();
                        int page = c.getInt("page");
                        int newpage = page +1;
                        plugin.gm.openTicketListMenu(player, newpage);
                        return;
                    }
                    if(action.equalsIgnoreCase("previous")){
                        event.setCancelled(true);
                        player.closeInventory();
                        int page = c.getInt("page");
                        int newpage = page - 1;
                        plugin.gm.openTicketListMenu(player, newpage);
                        return;
                    }
                } else {
                    event.setCancelled(true);
                }
            }
            event.setCancelled(true);
            return;
        }
        //TODO INVENTAIRE TICKET
        if (inventory.getName().contains("Ticket n°")) {
            net.minecraft.server.v1_12_R1.ItemStack nmsStack1 = CraftItemStack.asNMSCopy(clicked);
            if (nmsStack1.hasTag()) {
                net.minecraft.server.v1_12_R1.NBTTagCompound c = nmsStack1.getTag();
                if (c.hasKey("action")) {
                    String action = c.getString("action");
                    //TODO action : help

                    if(action.equalsIgnoreCase("teleport")){
                        event.setCancelled(true);
                        player.closeInventory();
                        int x = c.getInt("x");
                        int y = c.getInt("y");
                        int z = c.getInt("z");
                        String worldName = c.getString("w");
                        Location tpLoc = new Location(Bukkit.getWorld(worldName), x, y, z);
                        player.teleport(tpLoc);
                        return;
                    }
                    if(action.equalsIgnoreCase("refuse")){
                        event.setCancelled(true);
                        player.closeInventory();
                        int id = c.getInt("id");
                        plugin.tm.closeTicket(String.valueOf(id), player);
                        return;
                    }
                    //TODO action : make
                    //TODO action : manually
                    if(action.equalsIgnoreCase("ticketList")){
                        event.setCancelled(true);
                        player.closeInventory();
                        plugin.gm.openTicketListMenu(player, 1);
                        return;
                    }

                    if(action.equalsIgnoreCase("close")){
                        event.setCancelled(true);
                        player.closeInventory();
                        return;
                    }

                } else {
                    event.setCancelled(true);
                }
            }
            event.setCancelled(true);
            return;
        }
        else {
            return;
        }
    }

    @EventHandler
    public void onInvClose(InventoryCloseEvent event) {
        Player player = (Player) event.getPlayer();
        TicketObject ticket = plugin.tm.getTicketByOwner(player.getName());
        Inventory inventory = event.getInventory();
        if (inventory.getName().contains("Confirmation de la demande")) {
            if (ticket == null){
                HoloObject holo = plugin.hm.getHolo(player.getName());
                plugin.hm.deleteHolo(holo);
                plugin.um.removeClaimSelector(player);
            }
        }
    }
}
