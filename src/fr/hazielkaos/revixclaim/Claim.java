package fr.hazielkaos.revixclaim;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import fr.hazielkaos.revixclaim.commands.ClaimCmd;
import fr.hazielkaos.revixclaim.listeners.BlockListener;
import fr.hazielkaos.revixclaim.listeners.InventoryListener;
import fr.hazielkaos.revixclaim.listeners.PlayerListener;
import fr.hazielkaos.revixclaim.type.HoloObject;
import fr.hazielkaos.revixclaim.type.RegionObject;
import fr.hazielkaos.revixclaim.type.TicketObject;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

public class Claim extends JavaPlugin {
    private static Claim instance;
    public String version = "V2.0 beta ";
    public String prefix = "[RevixClaim] ";
    public File ticketsFile = null;
    public File holoFile = null;
    public File regionsFile = null;
    public YamlConfiguration tickets = new YamlConfiguration();
    public YamlConfiguration regions = new YamlConfiguration();
    public YamlConfiguration holo = new YamlConfiguration();
    public Permission permission = null;
    public static WorldGuardPlugin worldGuardPlugin;
    public static WorldEditPlugin worldEditPlugin;
    public UtilManager um = null;
    public GuiManager gm = null;
    public RegionManager rm = null;
    public HoloManager hm = null;
    public TicketManager tm = null;
    public BlockListener bListener = null;
    public PlayerListener pListener = null;
    public InventoryListener iListener = null;
    public ArrayList<TicketObject> ticketsList = null;
    public ArrayList<HoloObject> holoList = null;
    public ArrayList<RegionObject> regionList = null;

    public WorldGuardPlugin getWorldGuardPlugin() {
        return worldGuardPlugin;
    }

    public static WorldEditPlugin getWorldEditPlugin() {
        return worldEditPlugin;
    }

    @Override
    public void onEnable() {

        instance = this;
        ticketsList = new ArrayList<>();
        holoList = new ArrayList<>();
        regionList = new ArrayList<>();
        this.ticketsFile = new File(getDataFolder(), "ticket.yml");
        this.regionsFile = new File(getDataFolder(), "regions.yml");
        this.holoFile = new File(getDataFolder(), "holo.yml");
        um = new UtilManager(this);
        gm = new GuiManager(this);
        rm = new RegionManager(this);
        hm = new HoloManager(this);
        tm = new TicketManager(this);
        bListener = new BlockListener(this);
        pListener = new PlayerListener(this);
        iListener = new InventoryListener(this);
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(bListener, this);
        pm.registerEvents(pListener, this);
        pm.registerEvents(iListener, this);
        makeDirectory();
        loadYmls();


        worldGuardPlugin = (WorldGuardPlugin) getServer().getPluginManager().getPlugin("WorldGuard");
        worldEditPlugin = (WorldEditPlugin) getServer().getPluginManager().getPlugin("WorldEdit");
        if (!setupDepends()) {
            setEnabled(false);
        }
        if (!getDataFolder().exists()) {
            getDataFolder().mkdirs();
        }

        getCommand("claim").setExecutor(new ClaimCmd());
        loadTickets();
        loadHolo();
        loadRegions();
        getLogger().info("+-------------------------+");
        getLogger().info("| " + version + "has been activated! |");
        getLogger().info("+-------------------------+");

    }

    public void onDisable() {
        getLogger().warning(version + " has been desactivated!");
    }

    public static Claim get() {
        return instance;
    }

    private void makeDirectory() {
        if (!this.ticketsFile.exists()) {
            saveResource("ticket.yml", false);
        }
        if (!this.regionsFile.exists()) {
            saveResource("regions.yml", false);
        }
        if (!this.holoFile.exists()) {
            saveResource("holo.yml", false);
        }

    }

    private void loadYmls() {
        try {
            this.tickets.load(this.ticketsFile);
            this.regions.load(this.regionsFile);
            this.holo.load(this.holoFile);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public YamlConfiguration getTickets() {
        return this.tickets;
    }

    public void saveTickets() {
        try {
            this.tickets.save(this.ticketsFile);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public YamlConfiguration getRegions() {
        return this.regions;
    }

    public void saveRegions() {
        try {
            this.regions.save(this.regionsFile);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public YamlConfiguration getHolo() {
        return this.holo;
    }

    public void saveHolo() {
        try {
            this.holo.save(this.holoFile);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    private boolean setupDepends() {
        if (!setupPermission()) {
            getServer().getLogger()
                    .severe(version + "Le plugin de permissions est introuvable, etes-vous sur d'en avoir un ?");
            return false;
        }

        return true;
    }

    private Boolean setupPermission() {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager()
                .getRegistration(Permission.class);

        if (permissionProvider != null) {
            permission = (Permission) permissionProvider.getProvider();
            return true;
        }

        return false;
    }

    public void noperms(Player player) {
        player.sendTitle(ChatColor.DARK_RED + "Désolé", ChatColor.RED + "Vous n'avez pas la permission", 10, 70, 20);

    }

    public void loadTickets() {
        if (getTickets().getConfigurationSection("Tickets") != null) {
            Set<String> keys = getTickets().getConfigurationSection("Tickets").getKeys(false);
            int tempId = 0;
            getLogger().info("Chargement des tickets");
            for (String key : keys) {
                ConfigurationSection cs = getTickets().getConfigurationSection("Tickets." + key);
                tempId++;
                int id = Integer.parseInt(key);
                String uuid = cs.getString("UUID");
                String owner = cs.getString("PlayerName");
                String date = cs.getString("DateCreated");
                int x = cs.getInt("x");
                int y = cs.getInt("y");
                int z = cs.getInt("z");
                String world = cs.getString("world");
                int x1 = cs.getInt("x1");
                int y1 = cs.getInt("y1");
                int z1 = cs.getInt("z1");
                String w1 = cs.getString("w1");
                int x2 = cs.getInt("x2");
                int y2 = cs.getInt("y2");
                int z2 = cs.getInt("z2");
                String w2 = cs.getString("w2");
                TicketObject ticket = new TicketObject(id, uuid, owner, date, x, y, z, world, x1, y1, z1, w1, x2, y2, z2, w2, this);
            }
            getLogger().info(tempId + " Tickets chargés");

        }
    }


    public void loadHolo() {
        if (getHolo().getConfigurationSection("Holo") != null) {
            Set<String> keys = getHolo().getConfigurationSection("Holo").getKeys(false);
            int tempId = 0;
            getLogger().info("Chargement des hologrammes");
            for (String key : keys) {
                Location loc1;
                Location loc2;
                ConfigurationSection pos1 = getHolo().getConfigurationSection("Holo." + key + ".pos1");
                ConfigurationSection pos2 = getHolo().getConfigurationSection("Holo." + key + ".pos2");
                tempId++;
                String owner = key;

                if (pos1 != null) {
                    int x1 = pos1.getInt("x");
                    int y1 = pos1.getInt("y");
                    int z1 = pos1.getInt("z");
                    String w1 = pos1.getString("w");
                    loc1 = new Location(Bukkit.getWorld(w1), x1, y1, z1);
                } else {
                    loc1 = null;
                }
                if (pos2 != null) {
                    int x2 = pos2.getInt("x");
                    int y2 = pos2.getInt("y");
                    int z2 = pos2.getInt("z");
                    String w2 = pos2.getString("w");
                    loc2 = new Location(Bukkit.getWorld(w2), x2, y2, z2);
                } else {
                    loc2 = null;
                }
                HoloObject holo = new HoloObject(owner, loc1, loc2, this);

            }
            getLogger().info(tempId + " Hologrammes chargés");
        }
    }

    public void loadRegions() {
        if (getRegions().getConfigurationSection("Regions") != null) {
            Set<String> keys = getRegions().getConfigurationSection("Regions").getKeys(false);
            int tempId = 0;
            getLogger().info("Chargement des régions");
            for (String key : keys) {
                ConfigurationSection cs = getRegions().getConfigurationSection("Regions." + key);
                tempId++;
                String regionName = key;
                String ChestAccess = cs.getString("ChestAccess");
                String DoorOpen = cs.getString("DoorOpen");
                String ButtonUse = cs.getString("ButtonUse");
                String SwitchUse = cs.getString("SwitchUse");
                String FurnaceAccess = cs.getString("FurnaceAccess");
                String AnvilAccess = cs.getString("AnvilAccess");
                String FenceOpen = cs.getString("FenceOpen");
                String TrapOpen = cs.getString("TrapOpen");
                String AlambicAccess = cs.getString("AlambicAccess");
                String DispenserAccess = cs.getString("DispenserAccess");
                String HopperAccess = cs.getString("HopperAccess");
                String MinecartchestAccess = cs.getString("MinecartchestAccess");
                String MinecarthopperAccess = cs.getString("MinecarthopperAccess");
                RegionObject region = new RegionObject(regionName, ChestAccess, DoorOpen, ButtonUse, SwitchUse, FurnaceAccess, AnvilAccess, FenceOpen, TrapOpen, AlambicAccess, DispenserAccess, HopperAccess, MinecartchestAccess, MinecarthopperAccess, this);
            }
            getLogger().info(tempId + " Régions chargées");
        }
    }

    public void removeTicket(TicketObject ticket) {
        int id = ticket.getId();
        String node = "Tickets." + id;
        if (getTickets().getConfigurationSection(node) != null) {
            int ancienNb = getTickets().getInt("NbTicket");
            int newNb = ancienNb - 1;
            getTickets().set("NbTicket", newNb);
            getTickets().set(node, null);
            saveTickets();
        }
        ticketsList.remove(ticket);
    }

    public void removeHolo(HoloObject holo) {
        String owner = holo.getOwner();
        String node = "Holo." + owner;
        if (getHolo().getConfigurationSection(node) != null) {
            getHolo().set(node, null);
            saveHolo();
        }
        holoList.remove(holo);
    }

    public void removeRegions(RegionObject region) {
        String regionName = region.getRegionName();
        String node = "Regions." + regionName;
        if (getRegions().getConfigurationSection(node) != null) {
            getRegions().set(node, null);
            saveRegions();
        }
        regionList.remove(region);
    }

    public void saveHoloObject(HoloObject holo) {
        String owner = holo.getOwner();
        String node = "Holo." + owner;
        if (holo.getPos1() != null) {

            int x = holo.getPos1().getBlockX();
            int y = holo.getPos1().getBlockY();
            int z = holo.getPos1().getBlockZ();
            String w = holo.getPos1().getWorld().getName();
            getHolo().set(node + ".pos1.x", x);
            getHolo().set(node + ".pos1.y", y);
            getHolo().set(node + ".pos1.z", z);
            getHolo().set(node + ".pos1.w", w);
            getHolo().set(node + ".pos1.owner", owner);;
            saveHolo();
        }
        if (holo.getPos2() != null) {
            int x = holo.getPos2().getBlockX();
            int y = holo.getPos2().getBlockY();
            int z = holo.getPos2().getBlockZ();
            String w = holo.getPos2().getWorld().getName();
            getHolo().set(node + ".pos2.x", x);
            getHolo().set(node + ".pos2.y", y);
            getHolo().set(node + ".pos2.z", z);
            getHolo().set(node + ".pos2.w", w);
            getHolo().set(node + ".pos2.owner", owner);
            saveHolo();
        }
        if (holo.getPos1() != null && holo.getPos2() != null) {
            int x1 = holo.getPos1().getBlockX();
            int y1 = holo.getPos1().getBlockY();
            int z1 = holo.getPos1().getBlockZ();
            String w1 = holo.getPos1().getWorld().getName();
            int x2 = holo.getPos2().getBlockX();
            int y2 = holo.getPos2().getBlockY();
            int z2 = holo.getPos2().getBlockZ();
            String w2 = holo.getPos2().getWorld().getName();
            getHolo().set(node + ".pos1.x", x1);
            getHolo().set(node + ".pos1.y", y1);
            getHolo().set(node + ".pos1.z", z1);
            getHolo().set(node + ".pos1.w", w1);
            getHolo().set(node + ".pos1.owner", owner);
            getHolo().set(node + ".pos2.x", x2);
            getHolo().set(node + ".pos2.y", y2);
            getHolo().set(node + ".pos2.z", z2);
            getHolo().set(node + ".pos2.w", w2);
            getHolo().set(node + ".pos2.owner", owner);
            saveHolo();
        }
    }

    public void saveTicketObject(TicketObject ticket) {
        String node = "Tickets." + ticket.getId();
        getTickets().set(node + ".uuid", ticket.getUuid());
        getTickets().set(node + ".PlayerName", ticket.getOwner());
        getTickets().set(node + ".DateCreated", ticket.getDate());
        getTickets().set(node + ".x", ticket.getX());
        getTickets().set(node + ".y", ticket.getY());
        getTickets().set(node + ".z", ticket.getZ());
        getTickets().set(node + ".world", ticket.getWorld());
        getTickets().set(node + ".x1", ticket.getX1());
        getTickets().set(node + ".y1", ticket.getY2());
        getTickets().set(node + ".z1", ticket.getZ2());
        getTickets().set(node + ".x2", ticket.getX2());
        getTickets().set(node + ".y2", ticket.getY2());
        getTickets().set(node + ".z2", ticket.getZ2());
        saveTickets();
    }

    public void saveRegionsObject(RegionObject region) {
        String node = "Regions." + region.getRegionName();
        getRegions().set(node + ".ChestAccess", region.getChestAccess());
        getRegions().set(node + ".DoorOpen", region.getDoorOpen());
        getRegions().set(node + ".ButtonUse", region.getButtonUse());
        getRegions().set(node + ".SwitchUse", region.getSwitchUse());
        getRegions().set(node + ".FurnaceAccess", region.getFurnaceAccess());
        getRegions().set(node + ".AnvilAccess", region.getAnvilAccess());
        getRegions().set(node + ".FenceOpen", region.getFenceOpen());
        getRegions().set(node + ".TrapOpen", region.getTrapOpen());
        getRegions().set(node + ".AlambicAccess", region.getAlambicAccess());
        getRegions().set(node + ".DispenserAccess", region.getDispenserAccess());
        getRegions().set(node + ".HopperAccess", region.getHopperAccess());
        getRegions().set(node + ".MinecartchestAccess", region.getMinecartchestAccess());
        getRegions().set(node + ".MinecarthopperAccess", region.getMinecarthopperAccess());
        saveRegions();
    }

}
