package fr.hazielkaos.revixclaim.commands.sub;

import fr.hazielkaos.revixclaim.Claim;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;


public class TicketTpCommand extends ClaimSubCommand {

    public boolean execute(Player player, String[] values) {
        if (!Claim.get().getTickets().contains("Tickets." + values[1])) {
            player.sendMessage(ChatColor.RED + "~~~~~~~~~~ " + ChatColor.AQUA + Claim.get().prefix
                    + ChatColor.RED + "~~~~~~~~~~");
            player.sendMessage(ChatColor.GREEN + "Le ticket n'existe pas");
            return true;
        }
        player.sendMessage(ChatColor.AQUA + Claim.get().prefix + ChatColor.GREEN + "Teleportation en cours");
        int x = Claim.get().getTickets().getInt("Tickets." + values[1] + ".x");
        int y = Claim.get().getTickets().getInt("Tickets." + values[1] + ".y");
        int z = Claim.get().getTickets().getInt("Tickets." + values[1] + ".z");
        player.teleport(new Location(
                Bukkit.getWorld(Claim.get().getTickets().getString("Tickets." + values[1] + ".world")), x, y, z));
        return true;
    }

}
