package fr.hazielkaos.revixclaim.commands.sub;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import fr.hazielkaos.revixclaim.Claim;

public class InfoNameCommand extends ClaimSubCommand {
	private Claim plugin = Claim.get();
	@Override
	public boolean execute(Player player, String[] values) {
		WorldGuardPlugin worldGuard = plugin.getWorldGuardPlugin();
		
		OfflinePlayer op;
		String ownerList = "";
		String memberList = "";
		String ownerUUIDList = "";
		String memberUUIDList = "";
		RegionManager m = worldGuard.getRegionManager(player.getWorld());

		ProtectedRegion r = m.getRegion(values[1]);
		if (r == null) {
			player.sendMessage(ChatColor.RED + plugin.prefix + " la région " + values[1] + " n'existe pas");
			return true;
		}
		for (UUID ownerUUID : r.getOwners().getUniqueIds()) {
			op = Bukkit.getOfflinePlayer(ownerUUID);
			String name = op.getName();

			String strings[] = new String[] { name };
			for (String string : strings) {
				if (string != null) {
					ownerUUIDList += string + "*, ";
				}

			}

		}
		for (String ownerName : r.getOwners().getPlayers()) {

			String ownerStrings[] = new String[] { ownerName };
			for (String ownerString : ownerStrings) {
				ownerList += ownerString + ", ";
			}

		}
		for (UUID memberUUID : r.getMembers().getUniqueIds()) {
			op = Bukkit.getOfflinePlayer(memberUUID);
			String name = op.getName();

			String strings[] = new String[] { name };
			for (String string : strings) {
				if (string != null) {
					memberUUIDList += string + "*, ";
				}
			}
		}

		for (String memberName : r.getMembers().getPlayers()) {

			String memberStrings[] = new String[] { memberName };
			for (String memberString : memberStrings) {
				memberList += memberString + ", ";
			}

		}

		if (ownerList.length() >= 2) {
			ownerList = ownerList.substring(0, ownerList.length() - 2);
		}
		if (memberList.length() >= 2) {
			memberList = memberList.substring(0, memberList.length() - 2);
		}
		if (ownerUUIDList.length() >= 2) {
			ownerUUIDList = ownerUUIDList.substring(0, ownerUUIDList.length() - 2);
		}
		if (ownerList.length() == 0 && ownerUUIDList.length() == 0) {
			ownerList = "Il n'y a pas de propriétaire";
		}
		if (memberList.length() == 0 && memberUUIDList.length() == 0) {
			memberList = "Il n'y a pas de membre";
		}
		if (memberUUIDList.length() >= 2) {
			memberUUIDList = memberUUIDList.substring(0, memberUUIDList.length() - 2);
		}
		player.sendMessage(ChatColor.RED + "~~~~~~~~~~~~" + ChatColor.AQUA + "RevixClaim INFO" + ChatColor.RED
				+ "~~~~~~~~~~~~");
		player.sendMessage(ChatColor.GREEN + "Nom de la région : " + ChatColor.AQUA + values[1]);
		player.sendMessage(ChatColor.GREEN + "Propriétaire(s) :" + ChatColor.AQUA + " ".replace("&", "§")
				+ ownerList + " " + ownerUUIDList);
		player.sendMessage(ChatColor.GREEN + "Membre(s) :" + ChatColor.AQUA + " ".replace("&", "§") + memberList
				+ " " + memberUUIDList);
		return true;
		
	}

}
