package fr.hazielkaos.revixclaim.commands.sub;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import fr.hazielkaos.revixclaim.Claim;

public class AddCommand extends ClaimSubCommand {
	private Claim plugin = Claim.get();

	@Override
	public boolean execute(Player player, String[] values) {
		if (plugin.um.regionExist(values[1], player) == true) {
			if (plugin.um.isOwner(player, values[1]) == true) {
				plugin.um.addMember(values[1], values[2], player);
				
			} else {
				player.sendMessage(ChatColor.RED + plugin.prefix + "Vous n'êtes pas le propriétaire de la région " + values[1]);
				return false;
			}

		} else {
			player.sendMessage(ChatColor.RED + plugin.prefix + " la région " + values[1] + " n'existe pas");
			return false;
		}
		return false;
	}

}
