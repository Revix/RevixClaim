package fr.hazielkaos.revixclaim;

import fr.hazielkaos.revixclaim.type.HoloObject;
import fr.hazielkaos.revixclaim.type.TicketObject;
import net.md_5.bungee.api.ChatColor;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TicketManager {
    private Claim plugin;

    public TicketManager(Claim plugin) {
        this.plugin = plugin;
    }

    public boolean IsTicketRunning(Player player) {
        String p = player.getName();
        TicketObject ticket = getTicketByOwner(p);
        return ticket != null;
    }
    public TicketObject getTicketById(int id) {
        for (TicketObject ticket : plugin.ticketsList) {
            if (ticket.getId() == id) {
                return ticket;
            }
        }
        return null;
    }

    public TicketObject getTicketByOwner(String owner) {
        for (TicketObject ticket : plugin.ticketsList) {
            if (ticket.getOwner().equals(owner)) {
                return ticket;
            }
        }
        return null;
    }

    public TicketObject getTicket(String id) {
        if (StringUtils.isNumeric(id)) {
            int i = Integer.parseInt(id);
            return getTicketById(i);
        }
        return null;
    }

    public void createTicket(Player player) {
        if (IsTicketRunning(player)) {
            player.sendMessage(ChatColor.AQUA + plugin.prefix + ChatColor.GREEN + "Vous avez déja un ticket en attente, merci d'attendre le traitement du ticket précédent");
        } else {
            int ticketNumber = plugin.getTickets().getInt("TicketNumber");
            ticketNumber++;
            int NbTicket = plugin.getTickets().getInt("NbTicket");
            plugin.getTickets().set("TicketNumber", ticketNumber);
            plugin.getTickets().set("NbTicket", NbTicket + 1);
            plugin.saveTickets();
            Date now = new Date();
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");


            if (plugin.hm.hasDefinePos(player)) {
                // get ticket object information
                int id = ticketNumber;
                String uuid = player.getUniqueId().toString();
                String playerName = player.getName();
                String date = format.format(now);
                int x = player.getLocation().getBlockX();
                int y = player.getLocation().getBlockY();
                int z = player.getLocation().getBlockZ();
                String world = player.getLocation().getWorld().getName();
                //HOLO 1
                HoloObject holo = plugin.hm.getHolo(playerName);
                int x1 = holo.getPos1().getBlockX();
                int y1 = holo.getPos1().getBlockY();
                int z1 = holo.getPos1().getBlockZ();
                String worldName1 = holo.getPos1().getWorld().getName();
                //HOLO 2
                int x2 = holo.getPos2().getBlockX();
                int y2 = holo.getPos2().getBlockY();
                int z2 = holo.getPos2().getBlockZ();
                String worldName2 = holo.getPos2().getWorld().getName();
                TicketObject ticket = new TicketObject(id, uuid, playerName, date, x, y, z, world, x1, y1, z1, worldName1, x2, y2, z2, worldName2, plugin);
                plugin.saveTicketObject(ticket);
                player.sendMessage(ChatColor.AQUA + Claim.get().prefix + ChatColor.GREEN
                        + "Votre demande a bien été enregistrée a votre position sous le n° " + ticketNumber
                        + " elle serra traitée par un modérateur rapidement .");
                for (Player p : Bukkit.getServer().getOnlinePlayers()) {
                    if (plugin.permission.has(p, "claim.ticket")) {
                        p.sendTitle(ChatColor.DARK_GREEN + "Nouvelle demande de claim",
                                ChatColor.GREEN + "faite /claim ticket", 10, 70, 20);
                        p.sendMessage(ChatColor.RED + Claim.get().prefix + ChatColor.DARK_GREEN
                                + "Nouvelle demande de claim" + ChatColor.GREEN + " faite /claim ticket");
                    }
                }
            } else {
                player.sendMessage(ChatColor.RED + plugin.prefix + "Vous devez définir les positions de votre claim avant de créer un ticket");
            }
        }
    }


    public boolean isTicket() {
        int ticket = plugin.getTickets().getInt("NbTicket");
        return ticket != 0;
    }

    public void closeTicket(String ticketNumber, Player player) {

        TicketObject ticket = getTicket(ticketNumber);
        HoloObject holo = plugin.hm.getHolo(ticket.getOwner());
        plugin.hm.deleteHolo(holo);
        int ancienNb = plugin.getTickets().getInt("NbTicket");
        int newNb = ancienNb - 1;
        plugin.getTickets().set("NbTicket", newNb);
        plugin.getTickets().set("Tickets." + ticketNumber, null);
        plugin.saveTickets();
        plugin.removeTicket(ticket);
        player.sendMessage(ChatColor.RED + "~~~~~~~~~~ " + ChatColor.AQUA + plugin.prefix + ChatColor.RED
                + "~~~~~~~~~~");
        player.sendMessage(ChatColor.GREEN + "Le ticket a bien été fermé ");
    }


}
