package fr.hazielkaos.revixclaim;


import java.util.Arrays;
import java.util.LinkedList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
//import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.managers.storage.StorageException;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class UtilManager {

	private Claim plugin;

	public UtilManager(Claim plugin) {
		this.plugin = plugin;
	}

	public void InfoRegion(Player player) {
		WorldGuardPlugin worldGuard = plugin.getWorldGuardPlugin();
		Location location = player.getLocation();
		RegionManager regionManager = worldGuard.getRegionManager(location.getWorld());
		ApplicableRegionSet set = regionManager.getApplicableRegions(location);
		LinkedList<String> parentNames = new LinkedList<String>();
		LinkedList<String> regions = new LinkedList<String>();
		for (ProtectedRegion region : set) {
			String id = region.getId();
			regions.add(id);
			ProtectedRegion parent = region.getParent();
			while (parent != null) {
				parentNames.add(parent.getId());
				parent = parent.getParent();
			}
		}

		for (String name : parentNames) {
			regions.remove(name);

		}
		if (regions.isEmpty()) {

			player.sendMessage(ChatColor.RED + Claim.get().prefix + " il n'y a pas de region à votre emplacement");
			return;
		}

		OfflinePlayer op;
		String ownerList = "";
		String memberList = "";
		String ownerUUIDList = "";
		String memberUUIDList = "";
		RegionManager m = worldGuard.getRegionManager(player.getWorld());
		ProtectedRegion r = m.getRegion(regions.get(0));
		if (r == null) {
			player.sendMessage("region '" + regions.get(0) + "' does not exist");
			return;
		}
		for (UUID ownerUUID : r.getOwners().getUniqueIds()) {
			op = Bukkit.getOfflinePlayer(ownerUUID);
			String name = op.getName();

			String strings[] = new String[] { name };
			for (String string : strings) {
				if (string != null) {
					ownerUUIDList += string + "*, ";
				}

			}

		}
		for (String ownerName : r.getOwners().getPlayers()) {

			String ownerStrings[] = new String[] { ownerName };
			for (String ownerString : ownerStrings) {
				ownerList += ownerString + ", ";
			}

		}
		for (UUID memberUUID : r.getMembers().getUniqueIds()) {
			op = Bukkit.getOfflinePlayer(memberUUID);
			String name = op.getName();

			String strings[] = new String[] { name };
			for (String string : strings) {
				if (string != null) {
					memberUUIDList += string + "*, ";
				}
			}
		}
		for (String memberName : r.getMembers().getPlayers()) {

			String memberStrings[] = new String[] { memberName };
			for (String memberString : memberStrings) {
				memberList += memberString + ", ";
			}

		}
		if (ownerList.length() >= 2) {
			ownerList = ownerList.substring(0, ownerList.length() - 2);
		}
		if (memberList.length() >= 2) {
			memberList = memberList.substring(0, memberList.length() - 2);
		}
		if (ownerUUIDList.length() >= 2) {
			ownerUUIDList = ownerUUIDList.substring(0, ownerUUIDList.length() - 2);
		}
		if (ownerList.length() == 0 && ownerUUIDList.length() == 0) {
			ownerList = "Il n'y a pas de propriétaire";
		}
		if (memberList.length() == 0 && memberUUIDList.length() == 0) {
			memberList = "Il n'y a pas de membre";
		}
		if (memberUUIDList.length() >= 2) {
			memberUUIDList = memberUUIDList.substring(0, memberUUIDList.length() - 2);
		}
		player.sendMessage(
				ChatColor.RED + "~~~~~~~~~~~~" + ChatColor.AQUA + "RevixClaim INFO" + ChatColor.RED + "~~~~~~~~~~~~");
		player.sendMessage(ChatColor.GREEN + "Nom de la région : " + ChatColor.AQUA + regions.get(0));
		player.sendMessage(ChatColor.GREEN + "Propriétaire(s) :" + ChatColor.AQUA + " ".replace("&", "§") + ownerList
				+ " " + ownerUUIDList);
		player.sendMessage(ChatColor.GREEN + "Membre(s) :" + ChatColor.AQUA + " ".replace("&", "§") + memberList + " "
				+ memberUUIDList);
		return;
	}

	public boolean IsRegionHere(Location location) {
		WorldGuardPlugin worldGuard = plugin.getWorldGuardPlugin();
		RegionManager regionManager = worldGuard.getRegionManager(location.getWorld());
		ApplicableRegionSet set = regionManager.getApplicableRegions(location);
		LinkedList<String> parentNames = new LinkedList<String>();
		LinkedList<String> regions = new LinkedList<String>();
		for (ProtectedRegion region : set) {
			String id = region.getId();
			regions.add(id);
			ProtectedRegion parent = region.getParent();
			while (parent != null) {
				parentNames.add(parent.getId());
				parent = parent.getParent();
			}
		}
		for (String name : parentNames) {
			regions.remove(name);
		}

		if (regions.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}

	public String getRegionNameLoc(Location location) {
		WorldGuardPlugin worldGuard = plugin.getWorldGuardPlugin();
		RegionManager regionManager = worldGuard.getRegionManager(location.getWorld());
		ApplicableRegionSet set = regionManager.getApplicableRegions(location);
		LinkedList<String> parentNames = new LinkedList<String>();
		LinkedList<String> regions = new LinkedList<String>();
		for (ProtectedRegion region : set) {
			String id = region.getId();
			regions.add(id);
			ProtectedRegion parent = region.getParent();
			while (parent != null) {
				parentNames.add(parent.getId());
				parent = parent.getParent();
			}
		}
		for (String name : parentNames) {
			regions.remove(name);
		}

		return regions.get(0);
	}

	@SuppressWarnings("deprecation")
	public boolean CanOpenChest(Location location, Player player) {
		WorldGuardPlugin worldGuard = plugin.getWorldGuardPlugin();
		RegionManager regionManager = worldGuard.getRegionManager(location.getWorld());
		ApplicableRegionSet set = regionManager.getApplicableRegions(location);
		LinkedList<String> parentNames = new LinkedList<String>();
		LinkedList<String> regions = new LinkedList<String>();
		for (ProtectedRegion region : set) {
			String id = region.getId();
			regions.add(id);
			ProtectedRegion parent = region.getParent();
			while (parent != null) {
				parentNames.add(parent.getId());
				parent = parent.getParent();
			}
		}
		for (String name : parentNames) {
			regions.remove(name);
		}
		if (regions.isEmpty()) {
			return true;
		} else {
			RegionManager m = worldGuard.getRegionManager(player.getWorld());
			ProtectedRegion r = m.getRegion(regions.get(0));
			String playername = player.getName();
			if (r.isMember(playername) || r.isOwner(playername)) {
				return true;
			} else {
				return false;
			}
		}
	}

	@SuppressWarnings("deprecation")
	public boolean isOwner(Player player, String regionname) {
		WorldGuardPlugin worldGuard = plugin.getWorldGuardPlugin();
		String playerName = player.getName();
		RegionManager m = worldGuard.getRegionManager(player.getWorld());
		ProtectedRegion r = m.getRegion(regionname);
		if (r == null) {
			return false;
		}
		if (r.isOwner(playerName)) {
			return true;
		} else {
			return false;
		}
	}
	@SuppressWarnings("deprecation")
	public boolean isMember(Player player, String regionname) {
		WorldGuardPlugin worldGuard = plugin.getWorldGuardPlugin();
		String playerName = player.getName();
		RegionManager m = worldGuard.getRegionManager(player.getWorld());
		ProtectedRegion r = m.getRegion(regionname);
		if (r == null) {
			return false;
		}
		if (r.isMember(playerName)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean regionExist(String regionname, Player player) {
		WorldGuardPlugin worldGuard = plugin.getWorldGuardPlugin();
		RegionManager m = worldGuard.getRegionManager(player.getWorld());
		ProtectedRegion r = m.getRegion(regionname);
		if (r == null) {
			return false;
		} else {
			return true;
		}
	}

	public void addMember(String regionname, String membername, Player player) {
		WorldGuardPlugin worldGuard = plugin.getWorldGuardPlugin();
		RegionManager m = worldGuard.getRegionManager(player.getWorld());
		ProtectedRegion r = m.getRegion(regionname);
		DefaultDomain RegionMember = r.getMembers();
		RegionMember.addPlayer(membername);
		r.setMembers(RegionMember);
		// r.setFlag(DefaultFlag.GREET_MESSAGE, "coucou");
		// r.setFlag(DefaultFlag.FAREWELL_MESSAGE, "Aurevoir");
		try {
			m.save();
			player.sendMessage(ChatColor.RED + plugin.prefix + ChatColor.GREEN + "Vous avez ajouté " + membername
					+ " a votre claim : " + regionname);

			String ownername = player.getName();
			if (player.getServer().getPlayer(membername) != null) {
				Player member = Bukkit.getPlayer(membername);
				member.sendTitle(ChatColor.DARK_GREEN + "Vous avez été ajouté à", ChatColor.AQUA + regionname, 10, 70,
						20);
				member.sendMessage(ChatColor.AQUA + plugin.prefix + ChatColor.DARK_GREEN + ownername + ChatColor.GREEN
						+ " vous a ajouté à " + ChatColor.AQUA + regionname);
			}
		} catch (StorageException e) {
			e.printStackTrace();
			player.sendMessage(
					ChatColor.RED + plugin.prefix + "Quelque chose n'à pas marcher veuillez contacter HazielKaos");
		}
		return;
	}

	@SuppressWarnings("deprecation")
	public boolean memberexist(String regionname, String membername, Player player) {
		WorldGuardPlugin worldGuard = plugin.getWorldGuardPlugin();
		RegionManager m = worldGuard.getRegionManager(player.getWorld());
		ProtectedRegion r = m.getRegion(regionname);
		if (r == null) {
			return false;
		}
		if (r.isMember(membername)) {
			return true;
		} else {
			return false;
		}
	}

	public void removeMember(String regionname, String membername, Player player) {
		WorldGuardPlugin worldGuard = plugin.getWorldGuardPlugin();
		RegionManager m = worldGuard.getRegionManager(player.getWorld());
		ProtectedRegion r = m.getRegion(regionname);
		DefaultDomain RegionMember = r.getMembers();
		RegionMember.removePlayer(membername);
		r.setMembers(RegionMember);
		try {
			m.save();
			player.sendMessage(ChatColor.RED + plugin.prefix + ChatColor.GREEN + "Vous avez retiré " + membername
					+ " a votre claim : " + regionname);

			String ownername = player.getName();
			if (player.getServer().getPlayer(membername) != null) {
				Player member = Bukkit.getPlayer(membername);
				member.sendTitle(ChatColor.DARK_RED + "Vous avez été explusé de", ChatColor.AQUA + regionname, 10, 70,
						20);
				member.sendMessage(ChatColor.AQUA + plugin.prefix + ChatColor.DARK_RED + ownername + ChatColor.RED
						+ " vous a explusé de " + ChatColor.AQUA + regionname);
			}
		} catch (StorageException e) {
			e.printStackTrace();
			player.sendMessage(
					ChatColor.RED + plugin.prefix + "Quelque chose n'à pas marcher veuillez contacter HazielKaos");
		}
		return;
	}

	public boolean isInventoryIsFull(Player player) {
		Inventory inv = player.getInventory();
		for (ItemStack item : inv.getContents()) {
			if (item == null) {
				return false;
			}
		}
		return true;
	}

	public void addClaimSelector(Player player) {
		ItemStack[] inv = player.getInventory().getContents();
		for(ItemStack item:inv){
			if (item != null) {
				if (item.getType().equals(Material.STICK) && item.hasItemMeta()) {
					ItemMeta meta = item.getItemMeta();
					if (meta.getDisplayName().equalsIgnoreCase("Claim selector")) {
						player.getInventory().removeItem(item);
					}
				}
			}
		}
		ItemStack item = new ItemStack(Material.STICK);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName("Claim selector");
		meta.setLore(Arrays.asList("Clic droit pour selectionner le 1er emplacement",
				"Clic gauche pour selectionner le 2eme emplacement"));
		meta.addEnchant(Enchantment.LURE, 1, false);
		meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		item.setItemMeta(meta);
		player.getInventory().addItem(item);
	}
	public void removeClaimSelector(Player player){
		ItemStack[] inv = player.getInventory().getContents();
		for(ItemStack item:inv){
			if (item != null) {
				if (item.getType().equals(Material.STICK) && item.hasItemMeta()) {
					ItemMeta meta = item.getItemMeta();
					if (meta.getDisplayName().equalsIgnoreCase("Claim selector")) {
						player.getInventory().removeItem(item);
					}
				}
			}
		}
	}

	public boolean hasPlayerBypass(Player player){
		if(plugin.permission.has(player, "revix.bypass")){
			return true;
		}else {
			return false;
		}
	}



}
