package fr.hazielkaos.revixclaim.type;

import fr.hazielkaos.revixclaim.Claim;
import org.bukkit.Bukkit;
import org.bukkit.Location;


public class TicketObject {
    @SuppressWarnings("unused")
    private Claim plugin;

    public TicketObject(Claim plugin) {
        this.plugin = plugin;
    }

    // FIELDS
    protected int id;
    protected String uuid;
    protected String owner;
    protected String date;
    protected int x;
    protected int y;
    protected int z;
    protected String world;
    protected int x1;
    protected int y1;
    protected int z1;
    protected String w1;
    protected int x2;
    protected int y2;
    protected int z2;
    protected String w2;

    // CONSTRUCTOR
    public TicketObject(int id, String uuid, String owner, String date, int x, int y, int z, String world, int x1, int y1, int z1, String w1, int x2, int y2, int z2, String w2 , Claim plugin) {
        try {
            this.id = id;
            this.uuid = uuid;
            this.owner = owner;
            this.date = date;
            this.x = x;
            this.y = y;
            this.z = z;
            this.world = world;
            this.x1 = x1;
            this.y1 = y1;
            this.z1 = z1;
            this.w1 = w1;
            this.x2 = x2;
            this.y2 = y2;
            this.z2 = z2;
            this.w2 = w2;
            plugin.ticketsList.add(this);

        } catch (Exception ex) {
        }
    }

    //GETTER
    public int getId()
    {
        return id;
    }

    public String getUuid() {
        return uuid;
    }

    public String getOwner() {
        return owner;
    }
    public String getDate(){
        return date;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public String getWorld() {
        return world;
    }

    public int getX1() {
        return x1;
    }

    public int getY1() {
        return y1;
    }

    public int getZ1() {
        return z1;
    }

    public String getW1() {
        return w1;
    }

    public int getX2() {
        return x2;
    }

    public int getY2() {
        return y2;
    }

    public int getZ2() {
        return z2;
    }

    public String getW2() {
        return w2;
    }

    public Location getTpLoc(){
        return new Location(Bukkit.getWorld(getWorld()), x, y, z);
    }
    public Location getLoc1(){
        return new Location (Bukkit.getWorld(getW1()), x1, y1, z1);
    }
    public Location getLoc2(){
        return new Location(Bukkit.getWorld(getW2()), x2, y2, z2);
    }
}
