package fr.hazielkaos.revixclaim.commands;

import fr.hazielkaos.revixclaim.Claim;
import fr.hazielkaos.revixclaim.commands.sub.*;
import fr.hazielkaos.revixclaim.type.HoloObject;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ClaimCmd implements CommandExecutor {
    private Claim plugin = Claim.get();

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            ClaimSubCommand command;
            if (args.length == 0) {
                plugin.gm.openClaimMenu(player);
                return true;
            }
            if (args[0].equalsIgnoreCase("info")) {
                if (plugin.permission.has(player, "claim.info")) {
                    if (args.length == 1) {
                        command = new InfoCommand();
                        command.execute(player, args);
                        return true;
                    }
                    if (args.length == 2) {
                        command = new InfoNameCommand();
                        command.execute(player, args);
                        return true;
                    } else {
                        player.sendMessage(ChatColor.RED + plugin.prefix + ChatColor.GREEN + "Usage : /claim info (région en option)");
                        return true;
                    }
                }
            }
            if (args[0].equalsIgnoreCase("ticket")) {
                if (plugin.permission.has(player, "claim.ticket")) {
                    if (args.length == 1) {
                        command = new TicketCommand();
                        command.execute(player, args);
                        return true;
                    }

                    if (args.length == 2) {
                        command = new InfoTicketCommand();
                        command.execute(player, args);
                        return true;
                    } else {
                        player.sendMessage(ChatColor.RED + Claim.get().prefix + ChatColor.GREEN
                                + "Usage : /claim ticket ou /claim ticket <ID>");
                        return true;
                    }
                } else {
                    plugin.noperms(player);
                    return true;
                }
            }
            if (args[0].equalsIgnoreCase("tp")) {
                if (plugin.permission.has(player, "claim.tp")) {
                    if (args.length != 2) {
                        player.sendMessage(
                                ChatColor.RED + plugin.prefix + ChatColor.GREEN + "Usage : /claim tp <ID>");
                        return true;
                    } else {
                        command = new TicketTpCommand();
                        command.execute(player, args);
                        return true;
                    }
                } else {
                    plugin.noperms(player);
                    return true;
                }
            }
            if (args[0].equalsIgnoreCase("close")) {
                if (plugin.permission.has(player, "claim.close")) {
                    if (args.length != 2) {
                        player.sendMessage(
                                ChatColor.RED + plugin.prefix + ChatColor.GREEN + "Usage : /claim close <ID>");
                        return true;
                    } else {
                        command = new CloseCommand();
                        command.execute(player, args);
                        return true;
                    }
                } else {
                    plugin.noperms(player);
                    return true;
                }
            }
            if (args[0].equalsIgnoreCase("make")) {
                if (args.length != 2) {
                    player.sendMessage(
                            ChatColor.RED + plugin.prefix + ChatColor.GREEN + "Usage : /claim make <ID>");
                    return true;
                } else {
                    player.sendMessage(ChatColor.RED + plugin.prefix + ChatColor.GREEN + "Cette fonction n'est pas encore utilisable");
                    // on créer la région
                    return true;
                }
            }
            if (args[0].equalsIgnoreCase("add")) {
                if (args.length != 3) {
                    player.sendMessage(ChatColor.RED + plugin.prefix + ChatColor.GREEN
                            + "Usage : /claim add <nom de région> <joueur>");
                    return true;
                } else {
                    command = new AddCommand();
                    command.execute(player, args);
                    return true;
                }
            }
            if (args[0].equalsIgnoreCase("remove")) {
                if (args.length != 3) {
                    player.sendMessage(ChatColor.RED + plugin.prefix + ChatColor.GREEN
                            + "Usage : /claim remove <nom de région> <joueur>");
                    return true;
                } else {
                    command = new RemoveCommand();
                    command.execute(player, args);
                    return true;
                }

            }
            else {
                command = new HelpCommand();
                command.execute(player, args);
                return true;
            }
        }
        return false;
    }
}
